package dronfox.dailyfreshvender;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 11/17/2015.
 */
public class Login_Screen extends AppCompatActivity {
    Button btnLogin;
    Toolbar toolbar;
    EditText vendorPassword;
    EditText vendorId;

    MainRetrofitClass mainRetrofitClass;
SharedPrefrenceClass sharedPrefrenceClass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
sharedPrefrenceClass=new SharedPrefrenceClass(this);

        btnLogin = (Button) findViewById(R.id.Login);
        vendorId = (EditText) findViewById(R.id.vendor_Id);
        vendorPassword = (EditText) findViewById(R.id.vendorPassword);

      mainRetrofitClass=new MainRetrofitClass();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(Login_Screen.this, MainActivity.class);
//                startActivity(intent);
                if (vendorId.getText().toString().equals("")) {
                    vendorId.setError("Enter UserName");
                } else if (vendorPassword.getText().toString().equals("")) {
                    vendorPassword.setError("Enter Password");
                } else {

                    final ProgressDialog progressDialog=new ProgressDialog(Login_Screen.this);
                    progressDialog.setMessage("Please Wait");
                    progressDialog.show();
                    mainRetrofitClass.funRetro().login(vendorId.getText().toString(), vendorPassword.getText().toString(), new Callback<List<PojoClasss>>() {
                        @Override
                        public void success(List<PojoClasss> pojoClassses, Response response) {
                            sharedPrefrenceClass.setCheckLoginAccess("true");

                      for(int i=0;i<pojoClassses.size();i++)
                      {
                          sharedPrefrenceClass.setGetPlace(""+pojoClassses.get(i).getAreaAssigned());
                      }

                            Intent intent = new Intent(Login_Screen.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        @Override
                        public void failure(RetrofitError retrofitError) {
                            progressDialog.dismiss();
                            AlertDialog.Builder builder = new AlertDialog.Builder(Login_Screen.this);
                                builder.setTitle("Wrong UserName/Password");
                                builder.setMessage("Please Check Your Username/Password.");
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                builder.create();
                                builder.show();
                            }


                    });

                }
            }
        });
    }
}

