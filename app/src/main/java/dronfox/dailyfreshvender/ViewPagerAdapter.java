package dronfox.dailyfreshvender;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import dronfox.dailyfreshvender.ViewPagerFragementsPackage.Approval;
import dronfox.dailyfreshvender.ViewPagerFragementsPackage.Customer;
import dronfox.dailyfreshvender.ViewPagerFragementsPackage.Recharge;
import dronfox.dailyfreshvender.ViewPagerFragementsPackage.Today_Order;
import dronfox.dailyfreshvender.ViewPagerFragementsPackage.TommorowNeed;

/**
 * Created by Pardeep on 11/9/2015.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    String[] titles={"Customer","Orders","Tommorow Need","Approval","Recharge"};

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position)
        {
            case 0:
                return  new Customer();
            case 1:
                return  new Today_Order();
            case 2:
                return new TommorowNeed();
            case 3:
                return new Approval();
            case 4:
                return new Recharge();

        }


        return null;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
