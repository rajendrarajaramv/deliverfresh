package dronfox.dailyfreshvender;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

/**
 * Created by Pardeep on 12/28/2015.
 */
public class ShowCaseClass

{
    Context context;

    public ShowCaseClass(Context ctx) {
        context = ctx;
    }




    public void ShowCase(View view,Activity context,String dismissText,String contentText,String uniqueId)
    {

        new MaterialShowcaseView.Builder(context)
                .setTarget(view)

                .setDismissText(dismissText)
                .setContentText(contentText)
                .setDelay(1000) // optional but starting animations immediately in onCreate can make them choppy
                .singleUse(uniqueId) // provide a unique ID used to ensure it is only shown once
                .show();




        // sequence example
        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(500); // half second between each showcase view

        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(context,uniqueId);

        sequence.setConfig(config);

        sequence.addSequenceItem(view,
                "This is button one", "GOT IT");

        sequence.start();

    }









}

