package dronfox.dailyfreshvender;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Pardeep on 9/24/2015.
 */
public class SharedPrefrenceClass
{



    boolean todayOrder;
    boolean tomorrowOrder;
    boolean aproval;
    boolean recharge;
    public String cityName;

    public String getPhonenumber() {
        phonenumber=GetPrefrenc("phone");
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    SetPrefrences("phone",""+phonenumber);
    }

    String phonenumber;

    public String getCityName() {
        cityName=GetPrefrenc("cityName");
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
        SetPrefrences("cityName",""+cityName);
    }

    public String getGetPlace() {
        getPlace=GetPrefrenc("place");
        return getPlace;
    }

    public void setGetPlace(String getPlace) {
        this.getPlace = getPlace;
  SetPrefrences("place",getPlace);

    }

    String getPlace;


    public boolean isTodayOrder() {
       todayOrder=GetBoolPrefrenc("todayOrder");
        return todayOrder;
    }

    public void setTodayOrder(boolean todayOrder) {
        this.todayOrder = todayOrder;
        SetBooleanPrefrences("todayOrder",todayOrder);

    }

    public boolean isTomorrowOrder() {
      tomorrowOrder=GetBoolPrefrenc("tomorrowOrder");
        return tomorrowOrder;
    }

    public void setTomorrowOrder(boolean tomorrowOrder) {
        this.tomorrowOrder = tomorrowOrder;

        SetBooleanPrefrences("tomorrowOrder",tomorrowOrder);

    }

    public boolean isAproval() {
       aproval=GetBoolPrefrenc("aprove");
        return aproval;
    }

    public void setAproval(boolean aproval) {
        this.aproval = aproval;
    SetBooleanPrefrences("aprove",aproval);
    }

    public boolean isRecharge() {
        recharge=GetBoolPrefrenc("recharge");
        return recharge;
    }

    public void setRecharge(boolean recharge) {
        this.recharge = recharge;
    SetBooleanPrefrences("recharge",recharge);
    }

    public boolean isEditItem() {
     editItem=GetBoolPrefrenc("edit");
        return editItem;
    }

    public void setEditItem(boolean editItem) {
        this.editItem = editItem;
SetBooleanPrefrences("edit",editItem);
    }

    boolean editItem;



    public boolean isDialog_Customer() {
  dialog_Customer=GetBoolPrefrenc("dialogTrue");
        return dialog_Customer;
    }

    public void setDialog_Customer(boolean dialog_Customer) {
        this.dialog_Customer = dialog_Customer;
    SetBooleanPrefrences("dialogTrue",dialog_Customer);

    }

    boolean dialog_Customer;



    public void SetBooleanPrefrences(String key,boolean value)
    {


        SharedPreferences.Editor edt=preferences.edit();
        edt.putBoolean(key, value);
        edt.commit();



    }



    public static boolean  GetBoolPrefrenc(String key)
    {

        return  preferences.getBoolean(key, false);
    }







    Context ctx;

    public String getCheckLoginAccess() {
       checkLoginAccess=GetPrefrenc("login");
        return checkLoginAccess;
    }

    public void setCheckLoginAccess(String checkLoginAccess) {
        this.checkLoginAccess = checkLoginAccess;

   SetPrefrences("login",checkLoginAccess);

    }

    String checkLoginAccess;














    static SharedPreferences preferences;
    public SharedPrefrenceClass(Context ct)
    {
        ctx=ct;
        preferences= PreferenceManager.getDefaultSharedPreferences(ctx);

    }


    public static void SetPrefrences(String key, String value) {
        SharedPreferences.Editor edt=preferences.edit();
        edt.putString(key, value);
        edt.commit();
    }


    public static String  GetPrefrenc(String key)
    {
        return  preferences.getString(key, "none");
    }









}

