package dronfox.dailyfreshvender;

import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by Pardeep on 1/12/2016.
 */
public class MainRetrofitClass  {
   RestAdapter restAdapter ;
   RetrofitInterface retrofitInterface;

OkHttpClient okHttpClient;
    public RetrofitInterface funRetro()
    {


        okHttpClient=new OkHttpClient();
        okHttpClient.setConnectTimeout(30, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(30, TimeUnit.SECONDS);
        restAdapter = new RestAdapter.Builder().setEndpoint("http://deliverfresh.in").setClient(new OkClient(okHttpClient)).build();
        retrofitInterface = restAdapter.create(RetrofitInterface.class);
        restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);
    return  retrofitInterface;

    }

}
