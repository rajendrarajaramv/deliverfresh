package dronfox.dailyfreshvender;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import dronfox.dailyfreshvender.BaseadapterPackage.Baseadapter_ShowAllCustomers;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 11/19/2015.
 */
public class ShowAllCustomers extends AppCompatActivity
{
    LinearLayoutManager linearLayoutManager;
    RecyclerView recyclerView;
    String getIntent;

LinearLayout hidelayout;
    ArrayList<String> listName;
    ArrayList<String> listAddress;
    ArrayList<String> listContact;
NetworkClass networkClass;

MainRetrofitClass mainRetrofitClass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#169316")));
   setContentView(R.layout.recyclerviewlayout);

getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
mainRetrofitClass=new MainRetrofitClass();
        listName=new ArrayList<String>();
        listAddress=new ArrayList<String>();
        listContact=new ArrayList<String>();



        networkClass=new NetworkClass(this);
        hidelayout=(LinearLayout)findViewById(R.id.hidelayout);
        hidelayout.setVisibility(View.GONE);


        linearLayoutManager=new LinearLayoutManager(this);
        recyclerView=(RecyclerView)findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(linearLayoutManager);


        getIntent=getIntent().getStringExtra("apartmentName");
        getSupportActionBar().setTitle(getIntent);

if(networkClass.isConnectingToInternet()==true) {

    final ProgressDialog progressDialog=new ProgressDialog(this);
    progressDialog.setMessage("Fetching Customers");
    progressDialog.show();

    mainRetrofitClass.funRetro().CustomerRecord(getIntent, new Callback<List<PojoClasss>>() {
        @Override
        public void success(List<PojoClasss> pojoClassses, Response response) {

            for (int i = 0; i < pojoClassses.size(); i++) {
                String getName = pojoClassses.get(i).getName();
                String getFlat = pojoClassses.get(i).getFlat();
                String getTower = pojoClassses.get(i).getTower();
                String Aparment = pojoClassses.get(i).getApartment();
                String getContact = pojoClassses.get(i).getContactnumber();

                listName.add(getName);
                listAddress.add(getFlat + " , " + getTower + " , " + Aparment);
                listContact.add("" + getContact);

            }

            recyclerView.setAdapter(new Baseadapter_ShowAllCustomers(ShowAllCustomers.this, ShowAllCustomers.this, listName, listAddress, listContact));
        progressDialog.dismiss();
        }

        @Override
        public void failure(RetrofitError retrofitError) {

        }
    });
}
        else
{
    hidelayout.setVisibility(View.VISIBLE);
}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
finish();
                break;
        }
        return super.onOptionsItemSelected(item);


    }
}
