package dronfox.dailyfreshvender.ViewPagerFragementsPackage;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;

import dronfox.dailyfreshvender.Extra_Functions;
import dronfox.dailyfreshvender.MainRetrofitClass;
import dronfox.dailyfreshvender.R;
import dronfox.dailyfreshvender.SharedPrefrenceClass;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by Pardeep on 11/20/2015.
 */
public class Edit_AddCategory extends Fragment {

    EditText edtItemType;
    EditText edtBrandName;
    Spinner other_ItemName;
    EditText brandPrice;
    Button Add;
    String getCat;
    String getBrandName;
    String getVerName;
    String getPrice;
    SharedPrefrenceClass sharedPrefrenceClass;

    String getImageName;

    ArrayAdapter<String> adapter;
    String getType;
   MainRetrofitClass mainRetrofitClass;
    Extra_Functions functions;
    ImageView imageView;


    ImageView imgUpload;
    String[] variety={"Select Category","Bread","Butter","Dahi","Paneer","Cheese","Cream","Eggs","Milk","More"};
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.edit_addcategory, container, false);


            adapter=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_dropdown_item,variety);
        sharedPrefrenceClass=new SharedPrefrenceClass(getActivity());
        getType = getArguments().getString("type");
        imageView=(ImageView)view.findViewById(R.id.imageView);
        functions = new Extra_Functions(getActivity());
        imgUpload = (ImageView) view.findViewById(R.id.imgUpload);

        edtItemType = (EditText) view.findViewById(R.id.edtItemType);

        edtBrandName = (EditText) view.findViewById(R.id.edtBrandName);

        other_ItemName = (Spinner) view.findViewById(R.id.other_ItemName);
        other_ItemName.setAdapter(adapter);
        brandPrice = (EditText) view.findViewById(R.id.brandPrice);

        Add = (Button) view.findViewById(R.id.Add);




  mainRetrofitClass=new MainRetrofitClass();

        imgUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Choose Product Image");
                builder.setMessage("Please Select a way to pick the Product Image");
                builder.setPositiveButton("Gallery", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        try {
                            Intent gallery = new Intent(Intent.ACTION_PICK);
                            gallery.setType("image/*");
                            startActivityForResult(gallery, 0);

                        } catch (Exception e) {
                            }

                    }
                });


                builder.setNegativeButton("Camera", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            Intent camera = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(camera, 1);
                        } catch (Exception e) {

                        }
                    }
                });
                builder.create();
                builder.show();
            }
        });







        Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




                if(imageView.getTag()==null)
                {
                    Snackbar.make(getActivity().findViewById(android.R.id.content), "Please Insert Item Image", Snackbar.LENGTH_LONG).show();
                }

                else if (edtItemType.getText().toString().equals("")) {
                    edtItemType.setError("Enter Item Category");
                }
            else if (edtBrandName.getText().toString().equals("")) {
                    edtBrandName.setError("Enter Item Brand");
                }
                else if(other_ItemName.getSelectedItemPosition()==0)
                {
                    Toast.makeText(getActivity(),"Please Select Category",Toast.LENGTH_LONG).show();
                }
                else if (brandPrice.getText().toString().equals("")) {
                    brandPrice.setError("Enter Item Price");
                } else {

                    final ProgressDialog progressDialog=new ProgressDialog(getActivity());
                    progressDialog.setMessage("Posting Item.....");
progressDialog.show();
//                    mainRetrofitClass.funRetro().AddCategory(
//                            edtItemType.getText().toString(),
//                            edtBrandName.getText().toString(),
//                            ""+variety[other_ItemName.getSelectedItemPosition()],
//                            brandPrice.getText().toString(),
//                            ""+sharedPrefrenceClass.getCityName(),
//                            ""+sharedPrefrenceClass.getGetPlace(),
                           mainRetrofitClass.funRetro().AddCategory(""+other_ItemName.getSelectedItemPosition(),
                                   ""+edtItemType.getText().toString(),
                                   ""+variety[other_ItemName.getSelectedItemPosition()],
                                   ""+brandPrice.getText().toString(),
                                   ""+sharedPrefrenceClass.getCityName(),
                                ""+sharedPrefrenceClass.getGetPlace(),
                                   getImageName, ""+sharedPrefrenceClass.getPhonenumber(),new Callback<String>() {
                                @Override
                                public void success(String s, Response response) {


                                    imageView.setImageResource(R.drawable.mainicon);
                                  //  edtItemType.setText("");
                                    edtBrandName.setText("");
                                    other_ItemName.setSelection(0);
                                    brandPrice.setText("");
                                    progressDialog.dismiss();
                                    Toast.makeText(getActivity(),"Item Added",Toast.LENGTH_LONG).show();

                                }

                                @Override
                                public void failure(RetrofitError retrofitError) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getActivity(),"Item Not Added.Server Error. PLease Try Again",Toast.LENGTH_LONG).show();

                                }
                            }
                    );
                }
            }
        });


        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    getFragmentManager().popBackStackImmediate();
                }
                return false;
            }
        });
        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub






        if (requestCode == 1 && data != null) {

            try {
                Bitmap clicked_image = (Bitmap) data.getExtras().get("data");


                Uri tempUri = getImageUri(getActivity(), clicked_image);

                // CALL THIS METHOD TO GET THE ACTUAL PATH
                File finalFile = new File(getRealPathFromURI(tempUri));



                functions.compressImage("" + finalFile);


                Bitmap photo = BitmapFactory.decodeFile(functions.sdCardImagePath);


                imageView.setImageBitmap(photo);

                File f = new File(functions.sdCardImagePath);
                TypedFile typeFile = new TypedFile("application/octate-stream", f);

                getImageName="http://deliverfresh.in/API/NewApi/images/"+functions.serverfileName+".png";
                mainRetrofitClass.funRetro().insertImage(typeFile, new Callback<String>() {
                    @Override
                    public void success(String s, Response response) {
//                                Intent iop = new Intent(Registration.this, MainDashBoard.class);
//                                startActivity(iop);




                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {

                    }
                });


            } catch (Exception e) {
            }
        }
       else     if (requestCode == 0 && data != null) {
                try {

                    Uri photoUri = data.getData();
                    if (photoUri != null) {
                        try {
                            String[] filePathColumn = {MediaStore.Images.Media.DATA};
                            Cursor cursor = getActivity().getContentResolver().query(photoUri, filePathColumn, null, null, null);
                            cursor.moveToFirst();
                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            String filePath = cursor.getString(columnIndex);
                            cursor.close();


                            functions.compressImage(filePath);


                            Bitmap photo = BitmapFactory.decodeFile(functions.sdCardImagePath);


                              imageView.setImageBitmap(photo);

                            imageView.setTag("done");
                              File f = new File(functions.sdCardImagePath);
                            TypedFile typeFile = new TypedFile("application/octate-stream", f);


                            getImageName="http://deliverfresh.in/API/NewApi/images/"+functions.serverfileName+".png";
                            mainRetrofitClass.funRetro().insertImage(typeFile, new Callback<String>() {
                                @Override
                                public void success(String s, Response response) {
//                                Intent iop = new Intent(Registration.this, MainDashBoard.class);
//                                startActivity(iop);



                                    //  progressPic.setVisibility(View.GONE);

                                    //  Snackbar.make(findViewById(android.R.id.content),""+s,Snackbar.LENGTH_LONG).show();
                                }

                                @Override
                                public void failure(RetrofitError retrofitError) {
//                                progressPic.setVisibility(View.GONE);
//                                Snackbar.make(findViewById(android.R.id.content),"Image Uploaded Succussfully",Snackbar.LENGTH_LONG).show();
//
//
//                                Log.e("-------",""+retrofitError.getMessage());


                                }
                            });


                        } catch (Exception e) {

                        }
                    }
                } catch (Exception e) {

                }
            }
        }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }





}
