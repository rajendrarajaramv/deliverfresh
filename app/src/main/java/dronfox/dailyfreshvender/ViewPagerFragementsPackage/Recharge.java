package dronfox.dailyfreshvender.ViewPagerFragementsPackage;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import dronfox.dailyfreshvender.BaseadapterPackage.Baseadapter_Recharge;
import dronfox.dailyfreshvender.MainRetrofitClass;
import dronfox.dailyfreshvender.NetworkClass;
import dronfox.dailyfreshvender.PojoClasss;
import dronfox.dailyfreshvender.R;
import dronfox.dailyfreshvender.SharedPrefrenceClass;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 11/18/2015.
 */
public class Recharge extends Fragment
{

    LinearLayoutManager linearLayoutManager;
    RecyclerView recyclerView;


SharedPrefrenceClass sharedPrefrenceClass;
    ArrayList<String> listName;
    ArrayList<String> listAddress;
    ArrayList<String> listPrice;
    ArrayList<String>  listStatus;
    ArrayList<String> listTimeStamp;
    ArrayList<String> listDateTime;

    ArrayList<String> listContact;


    MainRetrofitClass mainRetrofitClass;


    NetworkClass networkClass;
    LinearLayout hidelayout;


    LinearLayout getHidelayout;
    ImageView mainImage;
    TextView textImage;
    TextView textTag;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recyclerviewlayout, container, false);


        ActionBar actionBar=((ActionBarActivity)getActivity()).getSupportActionBar();
        actionBar.setTitle("Recharge");



        sharedPrefrenceClass=new SharedPrefrenceClass(getActivity());

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(linearLayoutManager);

mainRetrofitClass=new MainRetrofitClass();

        getHidelayout=(LinearLayout)view.findViewById(R.id.hidelayout);
        mainImage=(ImageView)view.findViewById(R.id.mainImage);
        textImage=(TextView)view.findViewById(R.id.textImage);
        textTag=(TextView)view.findViewById(R.id.textTag);


        networkClass=new NetworkClass(getActivity());
        hidelayout=(LinearLayout)view.findViewById(R.id.hidelayout);
        hidelayout.setVisibility(View.GONE);



        listName=new ArrayList<String>();
        listAddress=new ArrayList<String>();
        listPrice=new ArrayList<String>();
        listStatus=new ArrayList<String>();
        listContact=new ArrayList<String>();
        listTimeStamp=new ArrayList<String>();
        listDateTime=new ArrayList<String>();

      if(networkClass.isConnectingToInternet()==true) {

          final ProgressDialog progressDialog=new ProgressDialog(getActivity());
          progressDialog.setMessage("Please Wait");
         progressDialog.show();
          mainRetrofitClass.funRetro().fetchRecharge_Display(""+sharedPrefrenceClass.getGetPlace(), new Callback<List<PojoClasss>>() {
              @Override
              public void success(List<PojoClasss> pojoClassses, Response response) {
                  for (int i = 0; i < pojoClassses.size(); i++) {


                      String getName = pojoClassses.get(i).getName();
                      String getContact = pojoClassses.get(i).getContactnumber();
                      String getFlat = pojoClassses.get(i).getAddress();;
                      String getAmount = pojoClassses.get(i).getAmount();
                      String getRechargeStatus = pojoClassses.get(i).getRechargeStatus();
                      String timestamp = pojoClassses.get(i).getTimestamp();
//                      String getDate = pojoClassses.get(i).getDatetime();

                      listName.add("" + getName);
                      listAddress.add("" + getFlat);
                      listPrice.add("" + getAmount + " /-");
                      listStatus.add("" + getRechargeStatus);
                      listContact.add("" + getContact);
                      listTimeStamp.add(timestamp);
//                      listDateTime.add("" + getDate);

                  }
                  recyclerView.setAdapter(new Baseadapter_Recharge(getActivity(), listName, listAddress, listPrice, listStatus, listContact, listTimeStamp, listDateTime));
              progressDialog.dismiss();

              }

              @Override
              public void failure(RetrofitError retrofitError) {
progressDialog.dismiss();


                  getHidelayout.setVisibility(View.VISIBLE);
                  mainImage.setImageResource(R.drawable.recharge);
                  textImage.setText("No Recharge Request");
                  textTag.setText("No Stock found for tommorow");

              }
          });

      }
        else
      {
          hidelayout.setVisibility(View.VISIBLE);
      }


        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if(keyCode==KeyEvent.KEYCODE_BACK)
                {
                    getFragmentManager().popBackStackImmediate();
                }
                return false;
            }
        });
        return  view;
    }









}


