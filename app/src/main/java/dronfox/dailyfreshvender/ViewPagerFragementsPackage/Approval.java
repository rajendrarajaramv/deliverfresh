package dronfox.dailyfreshvender.ViewPagerFragementsPackage;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import dronfox.dailyfreshvender.BaseadapterPackage.Baseadapter_Aproval;
import dronfox.dailyfreshvender.MainRetrofitClass;
import dronfox.dailyfreshvender.NetworkClass;
import dronfox.dailyfreshvender.PojoClasss;
import dronfox.dailyfreshvender.R;
import dronfox.dailyfreshvender.SharedPrefrenceClass;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 11/9/2015.
 */
public class Approval extends Fragment
{
    LinearLayoutManager linearLayoutManager;
    RecyclerView recyclerView;
MainRetrofitClass mainRetrofitClass;

String setStatus;
    ArrayList<String> listName;
    ArrayList<String> listTower;
    ArrayList<String> listApartment;
    ArrayList<String> listFlat;
    ArrayList<String> listStatus;
    ArrayList<String> listContact;

    SharedPrefrenceClass sharedPrefrenceClass;


    LinearLayout linearNoAproval;
    NetworkClass networkClass;
    LinearLayout hidelayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.aprovalneeded, container, false);

        ActionBar actionBar=((ActionBarActivity)getActivity()).getSupportActionBar();
        actionBar.setTitle("User Approvals");



        linearNoAproval=(LinearLayout)view.findViewById(R.id.linearNoAproval);
        linearNoAproval.setVisibility(View.GONE);
sharedPrefrenceClass=new SharedPrefrenceClass(getActivity());

        listName=new ArrayList<String>();
        listTower=new ArrayList<String>();
       listApartment=new ArrayList<String>();
       listFlat=new ArrayList<String>();
       listStatus=new ArrayList<String>();
       listContact=new ArrayList<String>();




     mainRetrofitClass=new MainRetrofitClass();
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(linearLayoutManager);


        networkClass=new NetworkClass(getActivity());
        hidelayout=(LinearLayout)view.findViewById(R.id.hidelayout);
        hidelayout.setVisibility(View.GONE);


        if(networkClass.isConnectingToInternet()==true) {
            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please Wait");
            progressDialog.show();
            mainRetrofitClass.funRetro().SelectAllStatus(""+sharedPrefrenceClass.getGetPlace(), new Callback<List<PojoClasss>>() {
                @Override
                public void success(List<PojoClasss> pojoClassses, Response response) {
                    for (int i = 0; i < pojoClassses.size(); i++) {
                        String getName = pojoClassses.get(i).getName();
                        String getTower = pojoClassses.get(i).getAddress();
                      //  String getApartment = pojoClassses.get(i).getApartment();
                      //  String getFlat = pojoClassses.get(i).getFlat();
                        String getStatus = pojoClassses.get(i).getIsUserTrue();
                        String getContact = pojoClassses.get(i).getContactnumber();

                        listName.add("" + getName);
                        listTower.add("" + getTower);
//                        listApartment.add("" + getApartment);
//                        listFlat.add("" + getFlat);


                        listStatus.add("" + getStatus);
                        listContact.add("" + getContact);

                    }
                    recyclerView.setAdapter(new Baseadapter_Aproval(getActivity(),
                            listName, listTower, listStatus, listContact));


                    progressDialog.dismiss();
                }

                @Override
                public void failure(RetrofitError retrofitError) {

                    linearNoAproval.setVisibility(View.VISIBLE);
                    Log.e("zfdf",""+retrofitError);
                    progressDialog.dismiss();
                }



            });
        }
        else
        {
            hidelayout.setVisibility(View.VISIBLE);
        }


//        view.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//
//                if(keyCode==KeyEvent.KEYCODE_BACK)
//                {
//                    getFragmentManager().popBackStackImmediate();
//                }
//                return false;
//            }
//        });
        return view;
    }








}
