package dronfox.dailyfreshvender.ViewPagerFragementsPackage;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import dronfox.dailyfreshvender.MainRetrofitClass;
import dronfox.dailyfreshvender.R;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 1/13/2016.
 */
public class EmergencyRecharge extends Fragment
{
    EditText mobileNumber;
    EditText Amount;
    Button rechaButton;
    MainRetrofitClass mainRetrofitClass;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.emergencyrc,container,false);

        ActionBar actionBar=((ActionBarActivity)getActivity()).getSupportActionBar();
        actionBar.setTitle("Emergency Recharge");
        mainRetrofitClass=new MainRetrofitClass();
        mobileNumber=(EditText)view.findViewById(R.id.mobileNumber);

        Amount=(EditText)view.findViewById(R.id.Amount);
        rechaButton=(Button)view.findViewById(R.id.recharge);


       rechaButton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               mainRetrofitClass.funRetro().Emergency(Amount.getText().toString(), mobileNumber.getText().toString(), new Callback<String>() {
                   @Override
                   public void success(String s, Response response) {


if(s.equals("done"))
{
    Amount.setText("");
    mobileNumber.setText("");
    Snackbar.make(getActivity().findViewById(android.R.id.content),"Recharge SuccessFul",Snackbar.LENGTH_LONG).dismiss();
}
                   }

                   @Override
                   public void failure(RetrofitError retrofitError) {

                   }
               });

           }
       });


        return view;
    }
}
