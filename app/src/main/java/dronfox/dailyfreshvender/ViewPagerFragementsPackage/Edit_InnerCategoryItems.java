package dronfox.dailyfreshvender.ViewPagerFragementsPackage;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import dronfox.dailyfreshvender.BaseadapterPackage.Baseadapter_InnerEdit;
import dronfox.dailyfreshvender.MainRetrofitClass;
import dronfox.dailyfreshvender.NetworkClass;
import dronfox.dailyfreshvender.PojoClasss;
import dronfox.dailyfreshvender.R;
import dronfox.dailyfreshvender.SharedPrefrenceClass;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 11/20/2015.
 */
public class Edit_InnerCategoryItems extends Fragment
{

    ArrayList<String> listBrandName;
    ArrayList<String> listBrandVeriety;
    ArrayList<String> listcategory;
    ArrayList<String> listcost;


   MainRetrofitClass mainRetrofitClass;
    LinearLayoutManager linearLayoutManager;
    RecyclerView recyclerView;
    NetworkClass networkClass;
    LinearLayout hidelayout;
SharedPrefrenceClass sharedPrefrenceClass;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.recyclerviewlayout,container,false);

       String getArg=getArguments().getString("bundleData");
     //   String getType=getArguments().getString("type");
sharedPrefrenceClass=new SharedPrefrenceClass(getActivity());


        networkClass=new NetworkClass(getActivity());
        hidelayout=(LinearLayout)view.findViewById(R.id.hidelayout);
        hidelayout.setVisibility(View.GONE);


       listBrandName=new ArrayList<String>();
       listBrandVeriety=new ArrayList<String>();
       listcategory=new ArrayList<String>();
       listcost=new ArrayList<String>();


     mainRetrofitClass=new MainRetrofitClass();
        linearLayoutManager=new LinearLayoutManager(getActivity());
        recyclerView=(RecyclerView)view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(linearLayoutManager);

if(networkClass.isConnectingToInternet()==true) {
    mainRetrofitClass.funRetro().ShowItemByCategory(getArg,""+sharedPrefrenceClass.getGetPlace(),""+sharedPrefrenceClass.getPhonenumber(),new Callback<List<PojoClasss>>() {
        @Override
        public void success(List<PojoClasss> pojoClassses, Response response) {
            for (int i = 0; i < pojoClassses.size(); i++) {
                String getBrandName = pojoClassses.get(i).getBrandname();
                String getCategory = pojoClassses.get(i).getCategory();
                String getBrandPrice = pojoClassses.get(i).getBrandPrice();
                String getBrandvrty = pojoClassses.get(i).getBrandVariety();

                listBrandName.add("" + getBrandName);
                listcategory.add("" + getCategory);
                listcost.add("" + getBrandPrice);
                listBrandVeriety.add("" + getBrandvrty);
            }

            recyclerView.setAdapter(new Baseadapter_InnerEdit(getActivity(), getActivity(),listBrandName, listBrandVeriety, listcategory, listcost));
        }

        @Override
        public void failure(RetrofitError retrofitError) {

        }
    });

}
        else
{
    hidelayout.setVisibility(View.VISIBLE);
}
        return  view;
    }
}
