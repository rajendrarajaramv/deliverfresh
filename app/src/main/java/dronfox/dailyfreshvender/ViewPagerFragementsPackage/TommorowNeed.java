package dronfox.dailyfreshvender.ViewPagerFragementsPackage;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import dronfox.dailyfreshvender.BaseadapterPackage.Baseadapter_TomorrowOrder;
import dronfox.dailyfreshvender.MainRetrofitClass;
import dronfox.dailyfreshvender.NetworkClass;
import dronfox.dailyfreshvender.PojoClasss;
import dronfox.dailyfreshvender.R;
import dronfox.dailyfreshvender.SharedPrefrenceClass;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 11/9/2015.
 */
public class TommorowNeed extends Fragment {

    LinearLayoutManager linearLayoutManager;
    RecyclerView recyclerView;
  MainRetrofitClass mainRetrofitClass;
    SharedPrefrenceClass sharedPrefrenceClass;

    ArrayList<String> listBrandName;
    ArrayList<String>listBrandItem;
    ArrayList<String>listCategory;
    ArrayList<String>listPrice;
NetworkClass networkClass;


LinearLayout getHidelayout;
    ImageView mainImage;
    TextView textImage;
    TextView textTag;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.recyclerviewlayout, container, false);


        ActionBar actionBar=((ActionBarActivity)getActivity()).getSupportActionBar();
        actionBar.setTitle("Tommorow's Need");

sharedPrefrenceClass=new SharedPrefrenceClass(getActivity());



        getHidelayout=(LinearLayout)view.findViewById(R.id.hidelayout);
        mainImage=(ImageView)view.findViewById(R.id.mainImage);
        textImage=(TextView)view.findViewById(R.id.textImage);
        textTag=(TextView)view.findViewById(R.id.textTag);







        mainRetrofitClass=new MainRetrofitClass();
        networkClass=new NetworkClass(getActivity());

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(linearLayoutManager);

        listBrandName=new ArrayList<String>();
        listBrandItem=new ArrayList<String>();
        listCategory=new ArrayList<String>();
        listPrice=new ArrayList<String>();


        //String getDate=new SimpleDateFormat("dd-MM-yyyy").format(new Date());

        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();

        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        String todayAsString = dateFormat.format(today);
        String getDate = dateFormat.format(tomorrow);




//        System.out.println(todayAsString);
//        System.out.println(tomorrowAsString);



        mainRetrofitClass.funRetro().TomorrowOrder(""+sharedPrefrenceClass.getGetPlace(), getDate, new Callback<List<PojoClasss>>() {
            @Override
            public void success(List<PojoClasss> pojoClassses, Response response) {
                for (int i = 0; i < pojoClassses.size(); i++) {
                    String type = pojoClassses.get(i).getOrderType();
                    String itemName = pojoClassses.get(i).getOrderBrandName();
                    String variety = pojoClassses.get(i).getOrderVariety();
                    String total = pojoClassses.get(i).getTotal();
                    listBrandName.add("" + itemName);
                    listBrandItem.add("" + variety);
                    listCategory.add("" + type);
                    listPrice.add("" + total);
                }

                recyclerView.setAdapter(new Baseadapter_TomorrowOrder(getActivity(), listBrandName, listBrandItem,
                        listCategory, listPrice));


              //  }
            }
            @Override
            public void failure(RetrofitError retrofitError) {
//                AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
//                builder.setTitle("No Order Found");
//                builder.setMessage("No orders for Tomorrow");
//                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                });
//                builder.create();
//                builder.show();


                getHidelayout.setVisibility(View.VISIBLE);
                mainImage.setImageResource(R.drawable.noorder);
                textImage.setText("No Stock Found");
                textTag.setText("No Stock found for tommorow");



            }
        });


        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    getFragmentManager().popBackStackImmediate();
                }
                return false;
            }
        });
        return view;

    }




    }



