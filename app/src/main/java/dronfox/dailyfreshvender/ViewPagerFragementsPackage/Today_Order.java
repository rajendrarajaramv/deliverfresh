package dronfox.dailyfreshvender.ViewPagerFragementsPackage;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dronfox.dailyfreshvender.BaseadapterPackage.Baseadapter_Orders;
import dronfox.dailyfreshvender.NetworkClass;
import dronfox.dailyfreshvender.PojoClasss;
import dronfox.dailyfreshvender.R;
import dronfox.dailyfreshvender.RetrofitInterface;
import dronfox.dailyfreshvender.SharedPrefrenceClass;
import dronfox.dailyfreshvender.TempDatabase.TodayOrderDatabase;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 11/9/2015.
 */
public class Today_Order extends Fragment {
    LinearLayoutManager linearLayoutManager;
    RecyclerView recyclerView;
 ArrayList<String> listOrderId;
    ArrayList<String> listOrderType;
    ArrayList<String> listOrderStatus;
    ArrayList<String> listOrderDate;
    ArrayList<String> listOrderArea;
    ArrayList<String> listOrderBrandName;
    ArrayList<String> listOrderVariety;
    ArrayList<String> listOrderQaunt;
    ArrayList<String> listName;
    ArrayList<String> listContact;
    ArrayList<String> listApartment;
    ArrayList<String> listTower;
    ArrayList<String> listFlat;
    ArrayList<String> listOrderBy;

    SharedPrefrenceClass sharedPrefrenceClass;

    RestAdapter restAdapter;
    RetrofitInterface retrofitInterface;

    LinearLayout getHidelayout;
    ImageView mainImage;
    TextView textImage;
    TextView textTag;

    TodayOrderDatabase todayOrderDatabase;
SQLiteDatabase sqLiteDatabase;
    NetworkClass networkClass;
    LinearLayout hidelayout;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recyclerviewlayout, container, false);


        sharedPrefrenceClass=new SharedPrefrenceClass(getActivity());

        ActionBar actionBar=((ActionBarActivity)getActivity()).getSupportActionBar();
        actionBar.setTitle("Today's Orders");

String dateGate=new SimpleDateFormat("dd-MM-yyyy").format(new Date());




        getHidelayout=(LinearLayout)view.findViewById(R.id.hidelayout);
        mainImage=(ImageView)view.findViewById(R.id.mainImage);
        textImage=(TextView)view.findViewById(R.id.textImage);
        textTag=(TextView)view.findViewById(R.id.textTag);



        todayOrderDatabase=new TodayOrderDatabase(getActivity());



        networkClass=new NetworkClass(getActivity());
        hidelayout=(LinearLayout)view.findViewById(R.id.hidelayout);
        hidelayout.setVisibility(View.GONE);


        restAdapter = new RestAdapter.Builder().setEndpoint("http://deliverfresh.in").build();
        retrofitInterface = restAdapter.create(RetrofitInterface.class);
        restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(linearLayoutManager);



listOrderId=new ArrayList<String>();
listOrderType=new ArrayList<String>();
listOrderStatus=new ArrayList<String>();
listOrderArea=new ArrayList<String>();
listOrderBrandName=new ArrayList<String>();
listOrderVariety=new ArrayList<String>();
listOrderQaunt=new ArrayList<String>();
listName=new ArrayList<String>();
listContact=new ArrayList<String>();
listApartment=new ArrayList<String>();
listTower=new ArrayList<String>();
        listOrderBy=new ArrayList<String>();
listFlat=new ArrayList<String>();
        listOrderDate=new ArrayList<String>();

        if(networkClass.isConnectingToInternet()==true) {
            String getDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

            retrofitInterface.FetchAllOrders(""+sharedPrefrenceClass.getGetPlace(), getDate, new Callback<List<PojoClasss>>() {
                @Override
                public void success(List<PojoClasss> pojoClassses, Response response) {


                    sqLiteDatabase = getActivity().openOrCreateDatabase("VendorDatabse", getActivity().MODE_PRIVATE, null);
                    String delete = "DELETE FROM todayOrder";
                    sqLiteDatabase.execSQL(delete);


                    for (int i = 0; i < pojoClassses.size(); i++) {


                        String getorderId = pojoClassses.get(i).getOrderId();
                        String getOrderBy = pojoClassses.get(i).getOrderBy();
                        String getOrderType = pojoClassses.get(i).getOrderType();
                        String getorderStatus = pojoClassses.get(i).getOrderStatus();
                        String getOrderArea = pojoClassses.get(i).getOrderArea();
                        String getBrandName = pojoClassses.get(i).getOrderBrandName();
                        String getBrandVrty = pojoClassses.get(i).getOrderVariety();
                        String getOrderQuant = pojoClassses.get(i).getOrderQaunt();
                        String getName = pojoClassses.get(i).getName();
                        String getCon = pojoClassses.get(i).getContactnumber();
                        String getApartment = pojoClassses.get(i).getAddress();
                        String getTower = pojoClassses.get(i).getTower();
                        String getFlat = pojoClassses.get(i).getFlat();

                        listOrderId.add(getorderId);
                        listOrderStatus.add(getorderStatus);
                        listName.add(getName);
                        listFlat.add(getFlat + "," + getTower + "," + getApartment);
                        listOrderBy.add("" + getOrderBy);
                        todayOrderDatabase.Insert(getorderId, getOrderType, getorderStatus, getOrderArea, getBrandName, getBrandVrty, getOrderQuant,
                                getName, getCon, getApartment, getTower, getFlat);


                    }



                    todayOrderDatabase.SelectUniquePerson(listContact,listName,listApartment,listOrderStatus,listOrderId);



                    recyclerView.setAdapter(new Baseadapter_Orders(getActivity(), listName,listApartment,listOrderStatus,listContact,listOrderId));


                }

                @Override
                public void failure(RetrofitError retrofitError) {

//
//                   AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
//                    builder.setTitle("No Order Found");
//                    builder.setMessage("No orders for today");
//                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//
//                        }
//                    });
//                    builder.create();
//                    builder.show();


                    getHidelayout.setVisibility(View.VISIBLE);
                    mainImage.setImageResource(R.drawable.noorder);
                    textImage.setText("No Order Found");
                    textTag.setText("No Orders Found For Today");

                }
            });

        }
        else
        {
            hidelayout.setVisibility(View.VISIBLE);
        }


        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if(keyCode==KeyEvent.KEYCODE_BACK)
                {
                    getFragmentManager().popBackStackImmediate();
                }
                return false;
            }
        });
        return  view;
    }




}

