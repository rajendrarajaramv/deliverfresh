package dronfox.dailyfreshvender.ViewPagerFragementsPackage;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import dronfox.dailyfreshvender.R;

/**
 * Created by Pardeep on 1/9/2016.
 */
public class EmergancyRefunds extends Fragment
{
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.recyclerviewlayout,container,false);
        return view;
    }
}
