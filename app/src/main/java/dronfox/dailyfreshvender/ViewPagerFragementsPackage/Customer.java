package dronfox.dailyfreshvender.ViewPagerFragementsPackage;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import dronfox.dailyfreshvender.BaseadapterPackage.BaseAdapter_Customer;
import dronfox.dailyfreshvender.MainRetrofitClass;
import dronfox.dailyfreshvender.NetworkClass;
import dronfox.dailyfreshvender.PojoClasss;
import dronfox.dailyfreshvender.R;
import dronfox.dailyfreshvender.SharedPrefrenceClass;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 11/9/2015.
 */
public class Customer extends Fragment {
    LinearLayoutManager linearLayoutManager;
    RecyclerView recyclerView;
   MainRetrofitClass mainRetrofitClass;
    ArrayList<String> apartName;
    ArrayList<String> apartPeople;
    ArrayList<String> amount;
    ArrayList<String> apartContact;

SharedPrefrenceClass sharedPrefrenceClass;
    LinearLayout hidelayout;
NetworkClass networkClass;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.recyclerviewlayout, container, false);
sharedPrefrenceClass=new SharedPrefrenceClass(getActivity());


        hidelayout=(LinearLayout)view.findViewById(R.id.hidelayout);



        ActionBar actionBar=((ActionBarActivity)getActivity()).getSupportActionBar();
        actionBar.setTitle("Customers");
        apartName = new ArrayList<String>();
        apartPeople = new ArrayList<String>();
        apartContact=new ArrayList<String>();
        amount=new ArrayList<String>();
     mainRetrofitClass=new MainRetrofitClass();

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(linearLayoutManager);

        networkClass=new NetworkClass(getActivity());
        hidelayout=(LinearLayout)view.findViewById(R.id.hidelayout);
        hidelayout.setVisibility(View.GONE);

if(networkClass.isConnectingToInternet()==true) {
    final ProgressDialog progressDialog = new ProgressDialog(getActivity());
    progressDialog.setMessage("Please Wait");
    progressDialog.show();


    mainRetrofitClass.funRetro().Customer_ShowAprtments(""+sharedPrefrenceClass.getGetPlace(), new Callback<List<PojoClasss>>() {
        @Override
        public void success(List<PojoClasss> pojoClassses, Response response) {
            for (int i = 0; i < pojoClassses.size(); i++) {
                String getApartment = pojoClassses.get(i).getName();
                String getTotal = pojoClassses.get(i).getAddress();
                String getContact=pojoClassses.get(i).getContactnumber();
String getAmount=pojoClassses.get(i).getAcountbalance();
                apartName.add("" + getApartment);
                apartPeople.add(getTotal);
            apartContact.add(getContact);
                amount.add("Rs "+getAmount);
            }

            recyclerView.setAdapter(new BaseAdapter_Customer(getActivity(),getActivity(), apartName, apartPeople,apartContact,amount));

            progressDialog.dismiss();
        }

        @Override
        public void failure(RetrofitError retrofitError) {
            hidelayout.setVisibility(View.VISIBLE);
            progressDialog.dismiss();
        }
    });
}
        else
{
    hidelayout.setVisibility(View.VISIBLE);
}



        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

               if(keyCode==KeyEvent.KEYCODE_BACK)
               {
                   getFragmentManager().popBackStackImmediate();
               }
                return false;
            }
        });
        return  view;
    }






}