package dronfox.dailyfreshvender.ViewPagerFragementsPackage;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import dronfox.dailyfreshvender.BaseadapterPackage.Baseadapter_EditItem;
import dronfox.dailyfreshvender.MainRetrofitClass;
import dronfox.dailyfreshvender.PojoClasss;
import dronfox.dailyfreshvender.R;
import dronfox.dailyfreshvender.SharedPrefrenceClass;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 11/20/2015.
 */
public class EditItems extends Fragment
{

    LinearLayout hideLayoutEdit;
    ArrayList<String> getCats;
    GridLayoutManager gridLayoutManager;
    RecyclerView recyclerView;
SharedPrefrenceClass sharedPrefrenceClass;
    MainRetrofitClass mainRetrofitClass;
    FloatingActionButton floatingAddCat;
    ArrayList<String> path;
    ArrayList<String> getGetCats;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.recycleredit, container, false);
        ActionBar actionBar=((ActionBarActivity)getActivity()).getSupportActionBar();
        actionBar.setTitle("Edit Item");

        hideLayoutEdit=(LinearLayout)view.findViewById(R.id.hideLayoutEdit);
        hideLayoutEdit.setVisibility(View.GONE);
getCats=new ArrayList<String>();
        mainRetrofitClass=new MainRetrofitClass();
path=new ArrayList<String>();
sharedPrefrenceClass=new SharedPrefrenceClass(getActivity());

      gridLayoutManager=new GridLayoutManager(getActivity(),3);
        recyclerView=(RecyclerView)view.findViewById(R.id.recyclerviewEdit);
            recyclerView.setLayoutManager(gridLayoutManager);
getGetCats=new ArrayList<String>();

       mainRetrofitClass.funRetro().edt_GetCategory(""+sharedPrefrenceClass.getGetPlace(),""+sharedPrefrenceClass.getPhonenumber(), new Callback<List<PojoClasss>>() {
           @Override
           public void success(List<PojoClasss> pojoClassses, Response response) {
               for (int i = 0; i < pojoClassses.size(); i++) {
                   String getCt = pojoClassses.get(i).getCatName();
                   String getcat=pojoClassses.get(i).getCategory();
                   String getImage=pojoClassses.get(i).getCatPic();
                   getCats.add("" + getCt);
                   getGetCats.add(""+getcat);
                   path.add(""+getImage);
               }

               recyclerView.setAdapter(new Baseadapter_EditItem(getActivity(), getCats,path,getGetCats));

           }

           @Override
           public void failure(RetrofitError retrofitError) {

               Log.e("Log",""+retrofitError);
hideLayoutEdit.setVisibility(View.VISIBLE);
           }
       });




        floatingAddCat=(FloatingActionButton)view.findViewById(R.id.floatingAddCat);
        floatingAddCat.setVisibility(View.VISIBLE);
        floatingAddCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment FragementSubCats = new Edit_AddCategory();
                Bundle bundle = new Bundle();
                bundle.putString("type", "insert");
                    FragementSubCats.setArguments(bundle);
                FragmentTransaction frag=getActivity().getSupportFragmentManager().beginTransaction();
                frag.replace(R.id.content_frame,FragementSubCats).addToBackStack(null);
                frag.commit();
            }
        });


        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    getFragmentManager().popBackStackImmediate();
                }
                return false;
            }
        });

        return view;
    }
}
