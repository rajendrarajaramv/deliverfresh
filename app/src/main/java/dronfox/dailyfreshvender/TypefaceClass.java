package dronfox.dailyfreshvender;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

/**
 * Created by Pardeep on 1/20/2016.
 */
public class TypefaceClass
{

    Typeface typeface;
    Context cont;
    public TypefaceClass(Context context)
    {
        cont=context;
        typeface=Typeface.createFromAsset(cont.getAssets(),"mons.otf");
    }

    public void setTypeface(TextView textView)
    {
        textView.setTypeface(typeface);
    }

}
