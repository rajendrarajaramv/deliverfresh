package dronfox.dailyfreshvender.BaseadapterPackage;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import dronfox.dailyfreshvender.MainRetrofitClass;
import dronfox.dailyfreshvender.R;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class Baseadapter_Aproval extends RecyclerView.Adapter<Baseadapter_Aproval.ViewHolder>
{

    Context context;

  //  TypefaceClass typefaceClass;

    ArrayList<String> listName;
    ArrayList<String> listTower;
    ArrayList<String> listApartment;
    ArrayList<String> listFlat;
    ArrayList<String> listStatus;
    ArrayList<String> listContact;

    MainRetrofitClass mainRetrofitClass;
    //Typeface typefaceClass;
    public Baseadapter_Aproval(Context c,
                                       ArrayList<String> Name,
                                       ArrayList<String> Tower,
                                       ArrayList<String> Status,
                                       ArrayList<String> Contact)
    {

        // TODO Auto-generated constructor stub
        context=c;
        listName=Name;
        listTower=Tower;

        listStatus=Status;
        listContact=Contact;

mainRetrofitClass=new MainRetrofitClass();

//ypefaceClass=new TypefaceClass(context);

//typefaceClass=Typeface.createFromAsset(context.getAssets(),"mons.otf");

    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
        // TODO Auto-generated method stub

        View inflaterview=LayoutInflater.from(arg0.getContext()).inflate(R.layout.baseadapter_aproval, arg0,false);
        ViewHolder vh=new ViewHolder(inflaterview);


        return vh;
    }



    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // TODO Auto-generated method stub

        holder.text_name.setText(""+listName.get(position));
        holder.text_Tower.setText(listTower.get(position));

//        holder.text_Tower.setTypeface(typefaceClass);
//        holder.text_name.setTypeface(typefaceClass);
        if(listStatus.get(position).equals("Pending"))

        {
            holder.text_Status.setText("" + listStatus.get(position));
        }
        else
        {
            holder.text_Status.setVisibility(View.GONE);
        }











holder.cardView.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {


        if (listStatus.get(position).equals("true"))
        {
            AlertDialog.Builder updateBuilder=new AlertDialog.Builder(context);
            updateBuilder.setTitle("Update");
            updateBuilder.setMessage("Sometimes a customer enter the wrong information," +
                    "A vendor can perform the following operations");
            updateBuilder.setNegativeButton("Update", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            updateBuilder.setPositiveButton("Delete User", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            updateBuilder.create();
            updateBuilder.show();
        }
        else {

            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Confirm Request");
            builder.setMessage("Before accepting the request,please make sure this person is available at the apartment");



            builder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    final ProgressDialog progressDialog=new ProgressDialog(context);
                    progressDialog.setMessage("Aproving Customer.....");
progressDialog.show();
                    mainRetrofitClass.funRetro().AproveRequest(listContact.get(position), new Callback<String>() {
                        @Override
                        public void success(String s, Response response) {



                        AlertDialog.Builder builder1=new AlertDialog.Builder(context);
                            builder1.setTitle("Done");
                            builder1.setMessage("Customer Request Aproved");
                            builder1.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

listStatus.set(position,"Valid");
                                    notifyDataSetChanged();

                                }
                            });
                            builder1.create();
                            builder1.show();

progressDialog.dismiss();
                        }

                        @Override
                        public void failure(RetrofitError retrofitError) {

                        }
                    });
                }
            });


            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });


            builder.create();
            builder.show();
        }
    }
});
    }
    @Override
    public int getItemCount() {
        // TODO Auto-generated method stub
        return listName.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder
    {

        CardView cardView;

        TextView text_name;
        TextView text_Tower;
        TextView text_Status;





        public ViewHolder(View itemView) {

            super(itemView);

        cardView=(CardView)itemView.findViewById(R.id.cardview);
            text_name=(TextView)itemView.findViewById(R.id.brandName);
            text_Tower=(TextView)itemView.findViewById(R.id.towerNumber);
            text_Status=(TextView)itemView.findViewById(R.id.textStatus);
        }

    }



}