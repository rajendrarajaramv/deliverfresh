package dronfox.dailyfreshvender.BaseadapterPackage;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import dronfox.dailyfreshvender.R;
import dronfox.dailyfreshvender.TypefaceClass;


public class Baseadapter_TomorrowOrder extends RecyclerView.Adapter<Baseadapter_TomorrowOrder.ViewHolder>
{

    Context context;

    ArrayList<String>listBrandName;
    ArrayList<String>listitemName;
    ArrayList<String>listCategory;
    ArrayList<String>listPrice;

TypefaceClass typefaceClass;
    public Baseadapter_TomorrowOrder(Context c,
                                     ArrayList<String>Name,
                                             ArrayList<String>itemName,
                                             ArrayList<String>category,
                                             ArrayList<String>price)
    {
        // TODO Auto-generated constructor stub
        context=c;


listBrandName=Name;
        listitemName=itemName;
        listCategory=category;
        listPrice=price;
typefaceClass=new TypefaceClass(context);
 }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
        // TODO Auto-generated method stub

        View inflaterview=LayoutInflater.from(arg0.getContext()).inflate(R.layout.baseadapter_tommoroworder, arg0,false);
        ViewHolder vh=new ViewHolder(inflaterview);


        return vh;
    }



    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position)
    {
        holder.tom_BrandName.setText(""+listBrandName.get(position));
        holder.tom_BrandItem.setText(""+listitemName.get(position));
        holder.tom_BrandCat.setText(""+listCategory.get(position));
        holder.tom_Price.setText(""+listPrice.get(position));
typefaceClass.setTypeface(holder.tom_BrandName);
        typefaceClass.setTypeface(holder.tom_BrandItem);
        typefaceClass.setTypeface(holder.tom_BrandCat);
        typefaceClass.setTypeface(holder.tom_Price);
    }
    @Override
    public int getItemCount() {
        // TODO Auto-generated method stub
        return listitemName.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder
    {

        TextView tom_BrandName;
        TextView tom_BrandItem;
        TextView tom_BrandCat;
        TextView tom_Price;

        public ViewHolder(View itemView) {

            super(itemView);


            tom_BrandName=(TextView)itemView.findViewById(R.id.tom_ItemBrand);
            tom_BrandItem=(TextView)itemView.findViewById(R.id.tom_ItemName);
            tom_BrandCat=(TextView)itemView.findViewById(R.id.tom_ItemCategory);
            tom_Price=(TextView)itemView.findViewById(R.id.tom_price);

        }

    }



}