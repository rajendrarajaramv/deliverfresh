package dronfox.dailyfreshvender.BaseadapterPackage;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import dronfox.dailyfreshvender.MainRetrofitClass;
import dronfox.dailyfreshvender.R;
import dronfox.dailyfreshvender.SharedPrefrenceClass;
import dronfox.dailyfreshvender.ShowCaseClass;
import dronfox.dailyfreshvender.TypefaceClass;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class Baseadapter_InnerEdit extends RecyclerView.Adapter<Baseadapter_InnerEdit.ViewHolder>
{

    Context context;


    ArrayList<String> listBrandName;
    ArrayList<String> listBrandVeriety;
    ArrayList<String> listcategory;
    ArrayList<String> listcost;
ShowCaseClass showCaseClass;

    MainRetrofitClass mainRetrofitClass;
    TypefaceClass typefaceClass;
    Activity act;
    SharedPrefrenceClass sharedPrefrenceClass;
    public Baseadapter_InnerEdit(Context c,Activity activity,ArrayList<String> name,ArrayList<String> vrty,
                                 ArrayList<String> category,ArrayList<String> cost)
    {

        // TODO Auto-generated constructor stub
        context=c;
listBrandName=name;
        act=activity;
        listBrandVeriety=vrty;
        listcategory=category;
        listcost=cost;
       mainRetrofitClass=new MainRetrofitClass();
showCaseClass=new ShowCaseClass(context);
        sharedPrefrenceClass=new SharedPrefrenceClass(context);
    typefaceClass=new TypefaceClass(context);
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
        // TODO Auto-generated method stub

        View inflaterview=LayoutInflater.from(arg0.getContext()).inflate(R.layout.baseadapter_inner, arg0,false);
        ViewHolder vh=new ViewHolder(inflaterview);


        return vh;
    }



    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // TODO Auto-generated method stub



holder.textbrandName.setText(""+listBrandName.get(position));
        holder.textVeriety.setText(""+listBrandVeriety.get(position));
        holder.price.setText(""+listcost.get(position));


        typefaceClass.setTypeface(holder.textbrandName);
        typefaceClass.setTypeface(holder.textVeriety);
        typefaceClass.setTypeface(holder.price);




       // showCaseClass.ShowCase(holder.price, act, "Close", "You can Update The price of the item which will be visible to all customer of your provided area","0");


holder.cardInner.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose Operation");
        builder.setMessage("Please Choose One Option");
        builder.setPositiveButton("Update Item", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                View viewDig = layoutInflater.inflate(R.layout.singleedittext, null);
                builder1.setView(viewDig);
                final EditText editText = (EditText) viewDig.findViewById(R.id.priceOld);
                editText.setText("" + listcost.get(position));
                builder1.setTitle("Update Price");
                builder1.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mainRetrofitClass.funRetro().BrandUpdatePrice(listBrandName.get(position), listBrandVeriety.get(position), editText.getText().toString(),""+sharedPrefrenceClass.getGetPlace(), new Callback<String>() {
                            @Override
                            public void success(String s, Response response) {
                                if (s.equals("SuccussFully Updated")) {
                                    listcost.set(position, editText.getText().toString());
                                    notifyDataSetChanged();
                                }
                            }

                            @Override
                            public void failure(RetrofitError retrofitError) {

                            }
                        });
                    }
                });
                builder1.create();
                builder1.show();
            }
        });
//        builder.setNegativeButton("Delete this item", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//retrofitInterface.deleteItem(listcategory.get(position), listBrandName.get(position), listBrandVeriety.get(position), new Callback<String>() {
//    @Override
//    public void success(String s, Response response) {
//
//    }
//
//    @Override
//    public void failure(RetrofitError retrofitError) {
//
//    }
//});
//            }
//        });

        builder.create();
        builder.show();
    }
});




    }
    @Override
    public int getItemCount() {
        // TODO Auto-generated method stub
        return listBrandName.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder
    {



TextView textbrandName;
        TextView textVeriety;
        TextView price;
CardView cardInner;
        public ViewHolder(View itemView) {

            super(itemView);

            textbrandName=(TextView)itemView.findViewById(R.id.textbrandName);
            textVeriety=(TextView)itemView.findViewById(R.id.textVeriety);
            price=(TextView)itemView.findViewById(R.id.price);
        cardInner=(CardView)itemView.findViewById(R.id.cardInner);

        }

    }



}