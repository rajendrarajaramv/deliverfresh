package dronfox.dailyfreshvender.BaseadapterPackage;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.cocosw.bottomsheet.BottomSheet;

import java.util.ArrayList;

import dronfox.dailyfreshvender.MainRetrofitClass;
import dronfox.dailyfreshvender.R;
import dronfox.dailyfreshvender.TypefaceClass;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class BaseAdapter_Customer extends RecyclerView.Adapter<BaseAdapter_Customer.ViewHolder>
{

    Context context;

    ArrayList<String>apartName;

    ArrayList<String>apartPeople;

    ArrayList<String>apartContact;
    ArrayList<String> amount;
    Activity activity;
    MainRetrofitClass mainRetrofitClass;
TypefaceClass typefaceClass;
    public BaseAdapter_Customer(Context c,Activity act,ArrayList<String> lstApart,ArrayList<String> lstPeople,
                                ArrayList<String> lstContact,
                                ArrayList<String> lstamount)
    {
        // TODO Auto-generated constructor stub
        context=c;
        apartName=  lstApart;
        apartPeople=lstPeople;
        activity=act;
        amount=lstamount;
        apartContact=lstContact;
    typefaceClass=new TypefaceClass(context);
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
        // TODO Auto-generated method stub

        View inflaterview=LayoutInflater.from(arg0.getContext()).inflate(R.layout.baseadapter_customer, arg0,false);
        ViewHolder vh=new ViewHolder(inflaterview);
    mainRetrofitClass=new MainRetrofitClass();

        return vh;
    }



    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // TODO Auto-generated method stub
holder.cardView.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View v) {
//        Intent intent=new Intent(context, ShowAllCustomers.class);
//        intent.putExtra("apartmentName",apartName.get(position));
//        context.startActivity(intent);


                                           new BottomSheet.Builder(activity).title("Choose Operation").sheet(R.menu.main).listener(new DialogInterface.OnClickListener() {


                                               @Override
                                               public void onClick(DialogInterface dialog, int which) {
                                                   switch (which) {
                                                       case R.id.call:
                                                           Intent intent = new Intent(Intent.ACTION_CALL);
                                                           intent.setData(Uri.parse("tel:" + apartContact.get(position)));
                                                           context.startActivity(intent);

                                                           break;


                                                       case R.id.delete:


                                                           AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                                           builder.setTitle("Are You Sure?");
                                                           builder.setMessage("You can delete the user and prevent him to order but still you will have the acount balance records");
                                                           builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                                                               @Override
                                                               public void onClick(DialogInterface dialog, int which) {

                                                                   mainRetrofitClass.funRetro().DeleteUser(apartContact.get(position), new Callback<String>() {
                                                                       @Override
                                                                       public void success(String s, Response response) {
                                                                           if (s.equals("succuss")) {


                                                                               notifyItemRemoved(position);

                                                                               apartName.remove(position);
                                                                               apartPeople.remove(position);

                                                                               notifyItemRangeChanged(0, apartContact.size());
                                                                              // notifyDataSetChanged();


                                                                               Toast.makeText(context, "Customer Deleted", Toast.LENGTH_LONG).show();
                                                                           }
                                                                       }

                                                                       @Override
                                                                       public void failure(RetrofitError retrofitError) {

                                                                       }
                                                                   });
                                                               }
                                                           });
                                                           builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                               @Override
                                                               public void onClick(DialogInterface dialog, int which) {

                                                               }
                                                           });
                                                           builder.create();
                                                           builder.show();

                                                           break;
                                                   }
                                               }
                                           }).show();
                                       }
                                   });
holder.totalPeople.setText("" + apartPeople.get(position));
        holder.textamount.setText(""+amount.get(position));
        holder.towerName.setText(""+apartName.get(position));

    typefaceClass.setTypeface(holder.totalPeople);

        typefaceClass.setTypeface(holder.textamount);

        typefaceClass.setTypeface(holder.towerName);

    }
    @Override
    public int getItemCount() {
        // TODO Auto-generated method stub
        return apartName.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder
    {
        CardView cardView;
        TextView towerName;
        TextView totalPeople;
        TextView textamount;
        public ViewHolder(View itemView) {
            super(itemView);
        cardView=(CardView)itemView.findViewById(R.id.cardview);

            towerName=(TextView)itemView.findViewById(R.id.personName);
            totalPeople=(TextView)itemView.findViewById(R.id.personAddress);
textamount=(TextView)itemView.findViewById(R.id.rs);
        }

    }



}