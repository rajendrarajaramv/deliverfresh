package dronfox.dailyfreshvender.BaseadapterPackage;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import dronfox.dailyfreshvender.MainRetrofitClass;
import dronfox.dailyfreshvender.R;
import dronfox.dailyfreshvender.TypefaceClass;
import dronfox.dailyfreshvender.ViewPagerFragementsPackage.Edit_InnerCategoryItems;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class Baseadapter_EditItem extends RecyclerView.Adapter<Baseadapter_EditItem.ViewHolder>
{

    Context context;
ArrayList<String> getListCats;
    ArrayList<String>listCategory;
MainRetrofitClass mainRetrofitClass;
    ArrayList<String> picPath;
TypefaceClass typefaceClass;
    public Baseadapter_EditItem(Context c,ArrayList<String> category,
                                ArrayList<String> pic,
                                ArrayList<String> getCats)
    {
        // TODO Auto-generated constructor stub
        context=c;

listCategory=category;
picPath=pic;
getListCats=getCats;
     mainRetrofitClass=new MainRetrofitClass();
typefaceClass=new TypefaceClass(context);
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
        // TODO Auto-generated method stub

        View inflaterview=LayoutInflater.from(arg0.getContext()).inflate(R.layout.baseadapter_edititems, arg0,false);
        ViewHolder vh=new ViewHolder(inflaterview);


        return vh;
    }



    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position)
    {
        Picasso.with(context).load(picPath.get(position)).into(holder.images);
holder.itemName.setText(""+listCategory.get(position));
        typefaceClass.setTypeface(holder.itemName);
        holder.cardEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment FragementSubCats = new Edit_InnerCategoryItems();
                Bundle bundle = new Bundle();
                bundle.putString("type","insert");
                bundle.putString("bundleData", getListCats.get(position));
                FragementSubCats.setArguments(bundle);
                FragmentTransaction tranc = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                tranc.replace(R.id.content_frame, FragementSubCats);
                tranc.commit();
            }
        });



holder.cardEdit.setOnLongClickListener(new View.OnLongClickListener() {
    @Override
    public boolean onLongClick(View v) {

        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        builder.setTitle("Choose Action");
        builder.setMessage("Please Choose one action");
        builder.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mainRetrofitClass.funRetro().deleteCategory(listCategory.get(position), new Callback<String>() {
                    @Override
                    public void success(String s, Response response) {

                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {

                    }
                });

                notifyItemRemoved(position);
                notifyItemRangeChanged(0,listCategory.size());


            }
        });
        builder.create();
        builder.show();

        return false;
    }
});

    }
    @Override
    public int getItemCount() {
        // TODO Auto-generated method stub
        return listCategory.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder
    {
TextView itemName;
        ImageView images;
CardView cardEdit;
        public ViewHolder(View itemView) {

            super(itemView);

itemName=(TextView)itemView.findViewById(R.id.itemName);
cardEdit=(CardView)itemView.findViewById(R.id.cardEdit);
        images=(ImageView)itemView.findViewById(R.id.images);
        }

    }



}