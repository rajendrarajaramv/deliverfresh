package dronfox.dailyfreshvender.BaseadapterPackage;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import dronfox.dailyfreshvender.R;
import dronfox.dailyfreshvender.TypefaceClass;


public class Baseadapter_ShowPersonOrder extends RecyclerView.Adapter<Baseadapter_ShowPersonOrder.ViewHolder>
{

    Context context;

    ArrayList<String>listBrandname;
    ArrayList<String>listBrandType;
    ArrayList<String>listBrandCount;

TypefaceClass typefaceClass;

    public Baseadapter_ShowPersonOrder(Context c,ArrayList<String> name,ArrayList<String> type,ArrayList<String> count)
    {
        // TODO Auto-generated constructor stub
        context=c;
listBrandname=name;
        listBrandType=type;
        listBrandCount=count;
        typefaceClass=new TypefaceClass(context);

    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
        // TODO Auto-generated method stub

        View inflaterview=LayoutInflater.from(arg0.getContext()).inflate(R.layout.baseadapter_itemdeliver, arg0,false);
        ViewHolder vh=new ViewHolder(inflaterview);


        return vh;
    }



    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position)
    {
holder.textBrandName.setText(""+listBrandname.get(position));
        holder.textBrandType.setText(""+listBrandType.get(position));
        holder.size.setText(""+listBrandCount.get(position));

    typefaceClass.setTypeface(holder.textBrandName);

        typefaceClass.setTypeface(holder.textBrandType);

        typefaceClass.setTypeface(holder.size);

    }
    @Override
    public int getItemCount() {
        // TODO Auto-generated method stub
        return listBrandname.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder
    {

        TextView textBrandName;
        TextView textBrandType;
        TextView size;

        public ViewHolder(View itemView) {

            super(itemView);

            textBrandName=(TextView)itemView.findViewById(R.id.textBrandName);
            textBrandType=(TextView)itemView.findViewById(R.id.textBrandType);
            size=(TextView)itemView.findViewById(R.id.size);

        }

    }



}