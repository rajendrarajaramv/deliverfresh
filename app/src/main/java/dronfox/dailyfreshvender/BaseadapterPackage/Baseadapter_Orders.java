package dronfox.dailyfreshvender.BaseadapterPackage;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import dronfox.dailyfreshvender.MainRetrofitClass;
import dronfox.dailyfreshvender.R;
import dronfox.dailyfreshvender.TempDatabase.TodayOrderDatabase;
import dronfox.dailyfreshvender.TypefaceClass;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class Baseadapter_Orders extends RecyclerView.Adapter<Baseadapter_Orders.ViewHolder>
{

    Context context;

    Display display;



    ArrayList<String> listOrderBrandName;
    ArrayList<String> listOrderVariety;
    ArrayList<String> listOrderQaunt;
    ArrayList<String> listOrderBy;


    ArrayList<String> listName;
    ArrayList<String> listAddress;
    ArrayList<String> liststatus;
    ArrayList<String> listcontactnumber;

    ArrayList<String> listOrderId;

TypefaceClass typefaceClass;
    TodayOrderDatabase todayOrderDatabase;
MainRetrofitClass mainRetrofitClass;
    public Baseadapter_Orders(Context c,
                              ArrayList<String> Name,
                              ArrayList<String> Address,
                              ArrayList<String> status,
                              ArrayList<String> contactnumber,
                              ArrayList<String> orderId)
    {

        // TODO Auto-generated constructor stub
        context=c;


listOrderId=orderId;
        listName=Name;
        listAddress=Address;
        liststatus=status;
        listcontactnumber=contactnumber;
        mainRetrofitClass=new MainRetrofitClass();

todayOrderDatabase=new TodayOrderDatabase(context);


        listOrderBrandName=new ArrayList<String>();
        listOrderVariety=new ArrayList<String>();
        listOrderQaunt=new ArrayList<String>();
typefaceClass=new TypefaceClass(context);
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
        // TODO Auto-generated method stub

        View inflaterview=LayoutInflater.from(arg0.getContext()).inflate(R.layout.baseadapter_orders, arg0,false);
        ViewHolder vh=new ViewHolder(inflaterview);


        return vh;
    }



    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // TODO Auto-generated method stub


        String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        holder.orderName.setText("" + listName.get(position));
        holder.orderApartment.setText("" + listAddress.get(position));
        holder.orderDate.setText("" + date);
        holder.status.setText("" + liststatus.get(position));

      typefaceClass.setTypeface(holder.orderName);

        typefaceClass.setTypeface(holder.orderApartment);

        typefaceClass.setTypeface(holder.orderDate);

        typefaceClass.setTypeface(holder.status);


        WindowManager windowManager = (WindowManager) context.getSystemService(context.WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();
        final int height = display.getHeight() / 2;

        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                todayOrderDatabase.SelectOrderList(listcontactnumber.get(position), listOrderBrandName, listOrderVariety, listOrderQaunt);

                final Dialog dig = new Dialog(context);
                dig.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                dig.setContentView(R.layout.dialog_showorders);
                dig.getWindow().setGravity(Gravity.BOTTOM);
                dig.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dig.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, height);
                TextView title = (TextView) dig.findViewById(R.id.title);
                FloatingActionButton floatView = (FloatingActionButton) dig.findViewById(R.id.floatView);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
                RecyclerView recyclerView = (RecyclerView) dig.findViewById(R.id.recyclerItemDeliver);
                recyclerView.setLayoutManager(linearLayoutManager);

title.setText(""+listName.get(position));
//
//
                recyclerView.setAdapter(new Baseadapter_ShowPersonOrder(context, listOrderBrandName, listOrderVariety, listOrderQaunt));


                floatView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        AlertDialog.Builder builder=new AlertDialog.Builder(context);
                        builder.setTitle("Deliver Order");
                        builder.setMessage("You are about to deliver the order to this customer, We will send a notification to the customer that you are going to dispatch the order");
                        builder.setPositiveButton("Dispatch", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                final ProgressDialog progressDialog=new ProgressDialog(context);
                                progressDialog.setMessage("Please Wait..");
                                progressDialog.show();

                                mainRetrofitClass.funRetro().DisplatchOrder(listOrderId.get(position), listcontactnumber.get(position), "Dispatched", "Ramesh", "9779032164", new Callback<String>() {
                                    @Override
                                    public void success(String s, Response response) {
if(s.equals("Done"))
{
    progressDialog.dismiss();
    Toast.makeText(context, "Notification Sent", Toast.LENGTH_LONG).show();
}
                                    }

                                    @Override
                                    public void failure(RetrofitError retrofitError) {

                                    }
                                });

                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });

builder.create();
                        builder.show();
                      //  Log.e("Log",""+listOrderId.get(position)+","+listOrderBy.get(position));

                    }
                });


                dig.show();


            }
        });
        }
        @Override
    public int getItemCount() {
        // TODO Auto-generated method stub
        return listName.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder
    {

        CardView cardview;
        TextView orderName;
        TextView orderApartment;
        TextView orderDate;


        TextView status;








        public ViewHolder(View itemView) {

            super(itemView);


             orderName=(TextView)itemView.findViewById(R.id.order_Name);
           orderApartment=(TextView)itemView.findViewById(R.id.orderApartment);
            orderDate=(TextView)itemView.findViewById(R.id.orderDate);

            cardview=(CardView)itemView.findViewById(R.id.cardview);
            status=(TextView)itemView.findViewById(R.id.status);

//             orderTower=(TextView)itemView.findViewById(R.id.orderTower);
//             orderFlat=(TextView)itemView.findViewById(R.id.orderFlat);
//             orderMilkType=(TextView)itemView.findViewById(R.id.orderMilkType);
//             orderFullCream=(TextView)itemView.findViewById(R.id.orderFull);
//             orderTonedMilk=(TextView)itemView.findViewById(R.id.orderToned);
//             orderButterMilk=(TextView)itemView.findViewById(R.id.orderButterMilk);
//             orderEggs=(TextView)itemView.findViewById(R.id.orderEggs);
//             orderBreads=(TextView)itemView.findViewById(R.id.orderBreads);



        }

    }



}