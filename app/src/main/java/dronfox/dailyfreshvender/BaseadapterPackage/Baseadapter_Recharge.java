package dronfox.dailyfreshvender.BaseadapterPackage;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import dronfox.dailyfreshvender.MainRetrofitClass;
import dronfox.dailyfreshvender.R;
import dronfox.dailyfreshvender.TypefaceClass;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class Baseadapter_Recharge extends RecyclerView.Adapter<Baseadapter_Recharge.ViewHolder>
{

    Context context;

    ArrayList<String> listName;
    ArrayList<String> listAddress;
    ArrayList<String> listPrice;
    ArrayList<String>  listStatus;
    ArrayList<String> listContact;
    ArrayList<String> listTimestamp;
    ArrayList<String> stamp;
MainRetrofitClass mainRetrofitClass;
    TypefaceClass typefaceClass;
    public Baseadapter_Recharge(Context c,
                                ArrayList<String> Name,
                                        ArrayList<String> Address,
                                        ArrayList<String> Price,
                                        ArrayList<String>  Status,
                                ArrayList<String> contact,
                                ArrayList<String> timestamp,ArrayList<String> time)
    {
        // TODO Auto-generated constructor stub
        context=c;

        listName=Name;
        listAddress=Address;
        listPrice=Price;
        listStatus=Status;
        listContact=contact;
listTimestamp=timestamp;
stamp=time;
        mainRetrofitClass=new MainRetrofitClass();


typefaceClass=new TypefaceClass(context);

    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
        // TODO Auto-generated method stub

        View inflaterview=LayoutInflater.from(arg0.getContext()).inflate(R.layout.baseadapter_recharge, arg0,false);
        ViewHolder vh=new ViewHolder(inflaterview);


        return vh;
    }



    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // TODO Auto-generated method stub


        holder.rc_Name.setText(""+listName.get(position));
        holder.rc_Address.setText(""+listAddress.get(position));
        holder.rc_Price.setText(""+listPrice.get(position));
        holder.rx_status.setText("" + listStatus.get(position));

        typefaceClass.setTypeface(holder.rc_Name);

        typefaceClass.setTypeface(holder.rc_Address);

        typefaceClass.setTypeface(holder.rc_Price);

        typefaceClass.setTypeface(holder.rx_status);



        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
if(listStatus.get(position).equalsIgnoreCase("complete"))
{

//    AlertDialog.Builder builder=new AlertDialog.Builder(context);
//    builder.setTitle("Recharge History");
// //   builder.setMessage(listName.get(position)+"\n"+"Contact Number \t\t"+listContact.get(position)+"\n"+"Date \t\t"+stamp.get(position));
//
//
//    builder.setMessage(listName.get(position)+"\n"+"Contact Number \t\t"+listContact.get(position)+"\n"+"Date \t\t"+stamp.get(position));
//
//
//    builder.create();
//    builder.show();

    Toast.makeText(context, "This Request is Already Completed", Toast.LENGTH_LONG).show();

}
else
{


                AlertDialog.Builder builder=new AlertDialog.Builder(context);
                builder.setTitle("Recharge");
                builder.setMessage("Make Sure you collect Money From Customer before recharge , Or send delivery boy to recieve the money the money");


    builder.setPositiveButton("Recharge", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            mainRetrofitClass.funRetro().doRecharge("" + listTimestamp.get(position), "" + listPrice.get(position), listContact.get(position), new Callback<String>() {
                @Override
                public void success(String s, Response response) {
                    Log.e("response",""+s);
if(s.equals("done"))
{
    listStatus.set(position,"Complete");
    notifyDataSetChanged();
}

                    Toast.makeText(context,"Successful Transection",Toast.LENGTH_LONG).show();

    }

    @Override
    public void failure(RetrofitError retrofitError) {

    }
});

                    }
                });


                builder.create();
                builder.show();

            }
            }
        });


    }
    @Override
    public int getItemCount() {
        // TODO Auto-generated method stub
        return listName.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder
    {

        CardView cardView;

        TextView rc_Name;
        TextView rc_Address;
        TextView rc_Price;
        TextView rx_status;
        public ViewHolder(View itemView) {

            super(itemView);

            rc_Name=(TextView)itemView.findViewById(R.id.rc_name);
            rc_Address=(TextView)itemView.findViewById(R.id.rec_address);
            rc_Price=(TextView)itemView.findViewById(R.id.rc_price);
            rx_status=(TextView)itemView.findViewById(R.id.rc_status);

cardView=(CardView)itemView.findViewById(R.id.cardviewRec);

        }

    }



}