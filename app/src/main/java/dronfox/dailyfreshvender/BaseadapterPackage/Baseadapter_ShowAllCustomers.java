package dronfox.dailyfreshvender.BaseadapterPackage;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cocosw.bottomsheet.BottomSheet;

import java.util.ArrayList;
import java.util.List;

import dronfox.dailyfreshvender.PojoClasss;
import dronfox.dailyfreshvender.R;
import dronfox.dailyfreshvender.RetrofitInterface;
import dronfox.dailyfreshvender.TypefaceClass;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class Baseadapter_ShowAllCustomers extends RecyclerView.Adapter<Baseadapter_ShowAllCustomers.ViewHolder>
{

    Context context;

ArrayList<String> listName;
    ArrayList<String> listAddress;
    ArrayList<String> listContact;
Activity activity;
RetrofitInterface retrofitInterface;
    RestAdapter restAdapter;
    TypefaceClass typefaceClass;
    public Baseadapter_ShowAllCustomers(Activity act,Context c, ArrayList<String> Name,ArrayList<String> Addr,ArrayList<String> contact)
    {
        // TODO Auto-generated constructor stub
        context=c;
listName=Name;
        listAddress=Addr;
activity=act;
        listContact=contact;

        restAdapter = new RestAdapter.Builder().setEndpoint("http://deliverfresh.in").build();
        retrofitInterface = restAdapter.create(RetrofitInterface.class);
        restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);
        typefaceClass=new TypefaceClass(context);
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
        // TODO Auto-generated method stub

        View inflaterview=LayoutInflater.from(arg0.getContext()).inflate(R.layout.baseadapter_showallcustomers, arg0,false);
        ViewHolder vh=new ViewHolder(inflaterview);


        return vh;
    }



    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // TODO Auto-generated method stub

holder.textName.setText(""+listName.get(position));
        holder.textAddress.setText(""+listAddress.get(position));

        typefaceClass.setTypeface(holder.textName);

        typefaceClass.setTypeface(holder.textAddress);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new BottomSheet.Builder(activity).title("Choose Operation").sheet(R.menu.main).listener(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        switch (which) {
                            case R.id.delete:


                                AlertDialog.Builder builder=new AlertDialog.Builder(context);
                                builder.setTitle("Are You Sure?");
                                builder.setMessage("You can delete the user and prevent him to order but still you will have the acount balance records");
                                builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        retrofitInterface.DeleteUser(listContact.get(position), new Callback<String>() {
                                            @Override
                                            public void success(String s, Response response) {

                                            }

                                            @Override
                                            public void failure(RetrofitError retrofitError) {

                                            }
                                        });

                                    }
                                });
                                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                builder.create();
                                builder.show();

                                break;
//                            case R.id.update:
//
//                                AlertDialog.Builder builder=new AlertDialog.Builder(context);
//                                LayoutInflater layoutInflater=(LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
//                                View view=layoutInflater.inflate(R.layout.dialog_update,null);
//                                builder.setView(view);
//
//                                EditText editText=(EditText)view.findViewById(R.id.updateName);
//                                EditText editFlat=(EditText)view.findViewById(R.id.updateFlat);
//
//                                editText.setText(""+listName.get(position));
//                             editFlat.setText(""+listAddress.get(position));
//
//                                builder.setTitle("Update Information");
//
//                                builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//
//                                    }
//                                });
//
//                                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//
//                                    }
//                                });
//                                builder.create();
//                                builder.show();
//
//                                break;
                            case R.id.call:
                                Intent intent=new Intent(Intent.ACTION_CALL);
                                intent.setData(Uri.parse("tel:"+listContact.get(position)));
                                context.startActivity(intent);


                                break;
                        }


                    }
                }).show();
            }
        });

    }
    @Override
    public int getItemCount() {
        // TODO Auto-generated method stub
        return listName.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder
    {

        CardView cardView;
        TextView textName;
        TextView textAddress;

        public ViewHolder(View itemView) {
            super(itemView);

            textName=(TextView)itemView.findViewById(R.id.text_Name);
            textAddress=(TextView)itemView.findViewById(R.id.textAddr);

cardView=(CardView)itemView.findViewById(R.id.cardview);
        }

    }



}