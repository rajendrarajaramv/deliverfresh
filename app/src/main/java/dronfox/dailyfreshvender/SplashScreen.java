package dronfox.dailyfreshvender;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by Pardeep on 11/13/2015.
 */
public class SplashScreen extends AppCompatActivity
{

    Button btnSignup;
    Button btnLogin;

    GPSTracker gpsTracker;
    ImageView mainLogo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      setContentView(R.layout.splashscreen);

        mainLogo=(ImageView)findViewById(R.id.mainlogo);
        mainLogo.setAnimation(AnimationUtils.loadAnimation(this,R.anim.elasticanimation));

        gpsTracker=new GPSTracker(this);
        if(gpsTracker.canGetLocation()==false)
        {
            gpsTracker.showSettingsAlert();
        }


     btnLogin=(Button)findViewById(R.id.btnLogin);
        btnSignup=(Button)findViewById(R.id.btnSignUp);
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SplashScreen.this,SignUpScreen.class);
                startActivity(intent);
                finish();
            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(SplashScreen.this,Login_Screen.class);
                startActivity(intent);
                finish();
            }
        });

    }
}
