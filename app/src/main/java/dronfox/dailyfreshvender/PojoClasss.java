package dronfox.dailyfreshvender;

/**
 * Created by Pardeep on 11/17/2015.
 */
public class PojoClasss
{

    String vendorName;
    String vendorContact;
    String vendorCity;

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorContact() {
        return vendorContact;
    }

    public void setVendorContact(String vendorContact) {
        this.vendorContact = vendorContact;
    }

    public String getVendorCity() {
        return vendorCity;
    }

    public void setVendorCity(String vendorCity) {
        this.vendorCity = vendorCity;
    }

    public String getAreaAssigned() {
        return areaAssigned;
    }

    public void setAreaAssigned(String areaAssigned) {
        this.areaAssigned = areaAssigned;
    }

    String areaAssigned;
    public String getAcountbalance() {
        return acountbalance;
    }

    public void setAcountbalance(String acountbalance) {
        this.acountbalance = acountbalance;
    }

    String acountbalance;
  String Brandname;
    String BrandVariety;

    public String getBrandname() {
        return Brandname;
    }

    public void setBrandname(String brandname) {
        Brandname = brandname;
    }

    public String getBrandVariety() {
        return BrandVariety;
    }

    public void setBrandVariety(String brandVariety) {
        BrandVariety = brandVariety;
    }

    public String getBrandPrice() {
        return BrandPrice;
    }

    public void setBrandPrice(String brandPrice) {
        BrandPrice = brandPrice;
    }

    public String getAvailCity() {
        return availCity;
    }

    public void setAvailCity(String availCity) {
        this.availCity = availCity;
    }

    public String getAvailArea() {
        return availArea;
    }

    public void setAvailArea(String availArea) {
        this.availArea = availArea;
    }

    String BrandPrice;
    String availCity;
    String availArea;

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    String orderBy;







    String orderQaunt;
    String orderVariety;

    public String getOrderQaunt() {
        return orderQaunt;
    }

    public void setOrderQaunt(String orderQaunt) {
        this.orderQaunt = orderQaunt;
    }

    public String getOrderVariety() {
        return orderVariety;
    }

    public void setOrderVariety(String orderVariety) {
        this.orderVariety = orderVariety;
    }

    public String getOrderBrandName() {
        return orderBrandName;
    }

    public void setOrderBrandName(String orderBrandName) {
        this.orderBrandName = orderBrandName;
    }

    String orderBrandName;

    String catPic;

    public String getCatPic() {
        return catPic;
    }

    public void setCatPic(String catPic) {
        this.catPic = catPic;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    String catName;




















    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    String category;


    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    String datetime;

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String cityName;
    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    String placeName;

    public String getPlace_name() {
        return place_name;
    }

    public void setPlace_name(String place_name) {
        this.place_name = place_name;
    }

    String place_name;


    public String getSumFullCream() {
        return SumFullCream;
    }

    public void setSumFullCream(String sumFullCream) {
        SumFullCream = sumFullCream;
    }

    public String getSumToned() {
        return SumToned;
    }

    public void setSumToned(String sumToned) {
        SumToned = sumToned;
    }

    public String getSumButterMilk() {
        return SumButterMilk;
    }

    public void setSumButterMilk(String sumButterMilk) {
        SumButterMilk = sumButterMilk;
    }

    String SumFullCream;
    String SumToned;
    String SumButterMilk;



String Amount;

    public String getTotal() {
        return Total;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    String address;
    public void setTotal(String total) {
        Total = total;
    }

    String Total;
    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    String timestamp;

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getRechargeStatus() {
        return rechargeStatus;
    }

    public void setRechargeStatus(String rechargeStatus) {
        this.rechargeStatus = rechargeStatus;
    }

    String rechargeStatus;






    String orderId;
     String  fullCreamQuant;
       String fullTonedQuant;
       String  butterMilkQuant;
        String eggsQuant;
    String BreadQuant;
    String orderDate;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getFullCreamQuant() {
        return fullCreamQuant;
    }

    public void setFullCreamQuant(String fullCreamQuant) {
        this.fullCreamQuant = fullCreamQuant;
    }

    public String getFullTonedQuant() {
        return fullTonedQuant;
    }

    public void setFullTonedQuant(String fullTonedQuant) {
        this.fullTonedQuant = fullTonedQuant;
    }

    public String getButterMilkQuant() {
        return butterMilkQuant;
    }

    public void setButterMilkQuant(String butterMilkQuant) {
        this.butterMilkQuant = butterMilkQuant;
    }

    public String getEggsQuant() {
        return eggsQuant;
    }

    public void setEggsQuant(String eggsQuant) {
        this.eggsQuant = eggsQuant;
    }

    public String getBreadQuant() {
        return BreadQuant;
    }

    public void setBreadQuant(String breadQuant) {
        BreadQuant = breadQuant;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getMilkType() {
        return milkType;
    }

    public void setMilkType(String milkType) {
        this.milkType = milkType;
    }

    public String getOrderArea() {
        return orderArea;
    }

    public void setOrderArea(String orderArea) {
        this.orderArea = orderArea;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderEnd() {
        return orderEnd;
    }

    public void setOrderEnd(String orderEnd) {
        this.orderEnd = orderEnd;
    }

    String milkType;
    String orderArea;
    String orderStatus;
    String orderType;
    String orderEnd;





    String name;
    String tower;
    String apartment;
    String flat;
    String isUserTrue;
    String contactnumber;






















    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTower() {
        return tower;
    }

    public void setTower(String tower) {
        this.tower = tower;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public String getFlat() {
        return flat;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public String getIsUserTrue() {
        return isUserTrue;
    }

    public void setIsUserTrue(String isUserTrue) {
        this.isUserTrue = isUserTrue;
    }



    public String getContactnumber() {
        return contactnumber;
    }

    public void setContactnumber(String contactnumber) {
        this.contactnumber = contactnumber;
    }




}
