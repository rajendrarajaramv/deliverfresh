package dronfox.dailyfreshvender;

import java.util.List;

import dronfox.dailyfreshvender.JsonLocation.PojoClassLocation;
import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * Created by Pardeep on 11/17/2015.
 */
public interface RetrofitInterface
{


    @Multipart
    @POST("/API/NewApi/upload.php")
    void insertImage(@Part("image") TypedFile typefile, Callback<String> ca);


    @FormUrlEncoded
    @POST("/API/NewApi/VendorSignUp.php")
    public void InsertData(@Field("vendorName") String vendorName,
                           @Field("vendorContact") String vendorContact,
                           @Field("vendorCity") String vendorCity,
                           @Field("vendorGcm") String vendorGcm,
                           @Field("areaAssigned") String area,
                           @Field("vendorPassword") String vendorPassword,
                           @Field("address") String address,
                           Callback<String> callback);






    @FormUrlEncoded
    @POST("/API/NewApi/CustomerStatus.php")
    public void SelectAllStatus(@Field("place") String place,Callback<List<PojoClasss>> listCallback);




    @FormUrlEncoded
    @POST("/API/NewApi/AproveCustomer.php")
    public void AproveRequest(@Field("contactnumber") String contact,Callback<String> callback);




    @FormUrlEncoded
    @POST("/API/NewApi/ShowAllOrders.php")
    public void FetchAllOrders(@Field("apartmentName") String orderArea,@Field("dateToday") String dateToday,Callback<List<PojoClasss>> listCallback);




    @FormUrlEncoded
    @POST("/API/NewApi/RechargeRequest_Display.php")
    public void fetchRecharge_Display(@Field("area") String area,Callback<List<PojoClasss>> listCallback);







    @FormUrlEncoded
    @POST("/API/NewApi/vendor_DoRecharge.php")
    public void doRecharge(@Field("timestamp")String timestamp,@Field("acountBalance") String amount,@Field("contactnumber") String contactnumber,Callback<String> callback );



    @FormUrlEncoded
    @POST("/API/NewApi/Customer_ShowApartments.php")
    public void Customer_ShowAprtments(@Field("areaAssigned")String areaAssigned,Callback<List<PojoClasss>> callback );



    @FormUrlEncoded
    @POST("/API/NewApi/CustomerRecords.php")
    public void CustomerRecord(@Field("apartmentName")String apartmentName,Callback<List<PojoClasss>> callback );


    @FormUrlEncoded
    @POST("/API/NewApi/vendor_DeleteCustomer.php")
    public void DeleteUser(@Field("contactnumber")String contactnumber,Callback<String> callback );



    @FormUrlEncoded
    @POST("/API/NewApi/TommorowsOrders.php")
    public void TomorrowOrder(@Field("apartmentName")String apartmentName,@Field("dateToday") String dateToday,Callback<List<PojoClasss>> callback );



    @POST("/API/NewApi/allPlace.php")
    public void fetchArea(Callback<List<PojoClasss>> listCallback);

@FormUrlEncoded
    @POST("/API/NewApi/vendor_Login.php")
    public void login(@Field("vendorContact") String contact,@Field("vendorPassword") String password,Callback<List<PojoClasss>> listCallback);



    @FormUrlEncoded
    @POST("/API/NewApi/Edit_ItemShowAllProducts.php")
    public void edt_GetCategory(@Field("availArea") String area,@Field("userId") String userId,Callback<List<PojoClasss>> callback);





    @FormUrlEncoded
    @POST("/API/NewApi/vendor_AddCategory.php")
    public void AddCategory(@Field("category") String category,
                            @Field("Brandname") String Brandname,
                            @Field("BrandVariety") String BrandVariety,
                            @Field("BrandPrice") String BrandPrice,
                            @Field("availCity") String availCity,
                            @Field("availArea") String availArea,
                            @Field("imagePath") String imagePath,
                            @Field("userId") String userId,
                             Callback<String> callback);





    @FormUrlEncoded
    @POST("/API/NewApi/vendor_ShowItemByCategory.php")
    public void ShowItemByCategory(@Field("category") String category,@Field("area") String area,@Field("userId") String userId, Callback<List<PojoClasss>> callback);



    @FormUrlEncoded
    @POST("/API/NewApi/vendor_DeleteCategory.php")
    public void deleteCategory(@Field("categoryName") String category, Callback<String> callback);




    @FormUrlEncoded
    @POST("/API/NewApi/vendor_DeleteItem.php")
    public void deleteItem(@Field("category") String categoryName,
                               @Field("Brandname") String Brandname,
                               @Field("BrandVariety") String BrandVariety,Callback<String> callback);





    @FormUrlEncoded
    @POST("/API/NewApi/vendor_UpdatePrice.php")
    public void BrandUpdatePrice(@Field("Brandname") String brandName,
                           @Field("BrandVariety") String brandVarie,
                           @Field("price") String price,
                                 @Field("area") String area,

                                 Callback<String> callback);








    @FormUrlEncoded
    @POST("/API/NewApi/vendor_DispatchOrder.php")
    public void DisplatchOrder(@Field("orderId") String orderId,
                                 @Field("orderBy") String orderBy,
                               @Field("orderStatus") String orderStatus,
                               @Field("vendorName") String name,
                               @Field("vendorContact") String contact,

                               Callback<String> callback);


    @FormUrlEncoded
    @POST("/API/NewApi/vendor_Holiday.php")
    public void PlaceHoliday(@Field("vendorArea") String vendorArea,
                               @Field("startDay") String startDay,
                               @Field("endDay") String endDay,
                               Callback<String> callback);




    @FormUrlEncoded
    @POST("/API/NewApi/EmergencyRc.php")
    public void Emergency(@Field("acountBalance")String acountBalance,
                           @Field("contactnumber")String contactnumber,
                           Callback<String> callback );






    @POST("/maps/api/geocode/json")
    public void FetchLocationGoogle(@Query("latlng") String latlng,@Query("sensor") String sensor,Callback<PojoClassLocation> pojoClassLocationCallback );




}
