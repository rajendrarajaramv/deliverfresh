package dronfox.dailyfreshvender;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.text.DecimalFormat;
import java.util.Date;

import dronfox.dailyfreshvender.ViewPagerFragementsPackage.Approval;
import dronfox.dailyfreshvender.ViewPagerFragementsPackage.Customer;
import dronfox.dailyfreshvender.ViewPagerFragementsPackage.EditItems;
import dronfox.dailyfreshvender.ViewPagerFragementsPackage.EmergencyRecharge;
import dronfox.dailyfreshvender.ViewPagerFragementsPackage.Recharge;
import dronfox.dailyfreshvender.ViewPagerFragementsPackage.Today_Order;
import dronfox.dailyfreshvender.ViewPagerFragementsPackage.TommorowNeed;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 8/28/2015.
 */
public class MainActivity extends ActionBarActivity //implements AdapterView.OnItemClickListener { ActionBarDrawerToggle actionBarDrawerToggle;

{
String startDay;
    String endDay;

    MainRetrofitClass mainRetrofitClass;
int click=0;


    SharedPrefrenceClass sharedPrefClass;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;
    FragmentTransaction fragmentTransaction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
mainRetrofitClass=new MainRetrofitClass();
        sharedPrefClass=new SharedPrefrenceClass(this);

        try {
            int getIntent = getIntent().getIntExtra("data",0);

TransectionUponNotifican(getIntent);
           }catch (Exception e)
        {

        }





        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.navigation);
        navigationView = (NavigationView) findViewById(R.id.navigation);
        View view = navigationView.inflateHeaderView(R.layout.headerlayout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerOpened(View drawerView) {
            }

            public void onDrawerClosed(View view) {
            }
        };

        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {

                fragmentTransaction = getSupportFragmentManager().beginTransaction();

                switch (item.getItemId()) {



                    case R.id.action_home:
                        fragmentTransaction.replace(R.id.content_frame, new Customer());
                        break;
                    case R.id.action_TodayOrder:
                        fragmentTransaction.replace(R.id.content_frame, new Today_Order());
                        break;
                    case R.id.action_TommorowOrder:
                        fragmentTransaction.replace(R.id.content_frame, new TommorowNeed());
                        break;
                    case R.id.actionApproval:
                        fragmentTransaction.replace(R.id.content_frame, new Approval());
                        break;
                    case R.id.actionRecharge:
                        fragmentTransaction.replace(R.id.content_frame, new Recharge());
                        break;
                    case R.id.actionEdit:
                        fragmentTransaction.replace(R.id.content_frame, new EditItems());
                        break;
                    case R.id.actionEmergency:
                        fragmentTransaction.replace(R.id.content_frame, new EmergencyRecharge());

                        break;
                    case R.id.actionVendorHoliday:
                        click = 0;
                        Dialog dialog = new Dialog(MainActivity.this);
                        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.calenderdialog);


                        final MaterialCalendarView calendarView = (MaterialCalendarView) dialog.findViewById(R.id.calendarView);
                        calendarView.setMinimumDate(new Date());
                        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
                            @Override
                            public void onDateSelected(MaterialCalendarView widget, CalendarDay date, boolean selected) {

                                click++;
                                DecimalFormat decimalFormat = new DecimalFormat("00");
                                switch (click) {
                                    case 1:


                                        int month = date.getMonth() + 1;
                                        startDay = "" + date.getYear() + "-" + decimalFormat.format(month) + "-" + decimalFormat.format(date.getDay());
                                        Snackbar.make(findViewById(android.R.id.content), "Please Choose the end date", Snackbar.LENGTH_LONG).show();

                                        break;
                                    case 2:
                                        int months = date.getMonth() + 1;
                                        endDay = "" + date.getYear() + "-" + decimalFormat.format(months) + "-" + decimalFormat.format(date.getDay());


                                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
                                        alertDialog.setTitle("Are You Sure?");
                                        alertDialog.setMessage("Do you really want ot place holiday from " + startDay + " to " + endDay + " ? ");
                                        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {


                                                mainRetrofitClass.funRetro().PlaceHoliday(sharedPrefClass.getGetPlace(), startDay, endDay, new Callback<String>() {
                                                    @Override
                                                    public void success(String s, Response response) {

                                                    }

                                                    @Override
                                                    public void failure(RetrofitError retrofitError) {

                                                    }
                                                });


                                            }
                                        });
                                        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        });
                                        alertDialog.create();
                                        alertDialog.show();


                                        break;

                                    default:

                                        break;
                                }


                            }
                        });


                        dialog.show();


                        break;
                }
                fragmentTransaction.commit();
                drawerLayout.closeDrawers();
                return false;
            }
        });


        fragmentTransaction=getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame,new Customer());
        fragmentTransaction.commit();
//
//    }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }


        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    protected void onRestart() {
        super.onRestart();

        // objLocalDb.SelectPersonalInfo(listName, listExper, listAvail, listprofilepic, listUserid);
//        Bitmap bm= BitmapFactory.decodeFile(listprofilepic.get(0));
//
//
//
//        circularImageView.setImageBitmap(bm);
//        textView.setText("" + listName.get(0));



    }



    public void TransectionUponNotifican(int code)
    {


        fragmentTransaction=getSupportFragmentManager().beginTransaction();

switch (code) {


    case 0:
    fragmentTransaction.replace(R.id.content_frame, new Approval());
break;
    case 1:
        fragmentTransaction.replace(R.id.content_frame, new Recharge());
        break;

}
        fragmentTransaction.commit();
    }
}
