package dronfox.dailyfreshvender;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.util.ArrayList;
import java.util.List;

import dronfox.dailyfreshvender.JsonLocation.PojoClassLocation;
import dronfox.dailyfreshvender.TempDatabase.TempDb;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 11/13/2015.
 */
public class SignUpScreen extends AppCompatActivity {
    GoogleCloudMessaging googleCloudMessaging;
    public static String getGcmId;
    public static String projectNumber="60095088152";

    SharedPrefrenceClass sharedPrefrenceClass;

    Spinner spinnerCity;
    Spinner spinnerArea;
    ArrayAdapter<String> adapterCity;
    ArrayAdapter<String> adapterArea;


    TempDb tempDb;
MainRetrofitClass mainRetrofitClass;
    ArrayList<String> listCity;
    ArrayList<String> listArea;

 EditText editName;
    EditText editContact;
    EditText editPassword;
EditText vendorAddress;
    String getCity;
    String getLocationString;
    String getVendorArea;
GPSTracker gpsTracker;
Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
tempDb=new TempDb(this);
        sharedPrefrenceClass=new SharedPrefrenceClass(this);
        googleCloudMessaging=GoogleCloudMessaging.getInstance(this);
        new AsynGcm().execute();

        mainRetrofitClass=new MainRetrofitClass();


        gpsTracker=new GPSTracker(this);
vendorAddress=(EditText)findViewById(R.id.vendorAddress);


        editName=(EditText)findViewById(R.id.vendor_userName);
        editContact=(EditText)findViewById(R.id.vendor_Contactnumber);
        editPassword=(EditText)findViewById(R.id.vendor_password);

button=(Button)findViewById(R.id.buttonSignup);


        listArea=new ArrayList<String>();
        listCity=new ArrayList<String>();
        spinnerCity=(Spinner)findViewById(R.id.cityAvail);
        spinnerArea=(Spinner)findViewById(R.id.AreaAvail);
    adapterCity=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,listCity);
        adapterArea=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,listArea);




        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("http://maps.googleapis.com").build();
        restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);
        RetrofitInterface retrofitInterface = restAdapter.create(RetrofitInterface.class);
        String latlong = "" + gpsTracker.getLatitude() + "," + gpsTracker.getLongitude();
        retrofitInterface.FetchLocationGoogle(latlong, "true", new Callback<PojoClassLocation>() {
            @Override
            public void success(PojoClassLocation pojoClassLocation, Response response) {
                getLocationString = "" + pojoClassLocation.getResults().get(0).getFormatted_address();
                //locationString.setText("" + getLocationString);
                //  progressDialog.dismiss();


vendorAddress.setText(""+getLocationString);
            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });




        final ProgressDialog progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Fetching Place Details");
        progressDialog.show();

        tempDb.DELETE();
        mainRetrofitClass.funRetro().fetchArea(new Callback<List<PojoClasss>>() {
            @Override
            public void success(List<PojoClasss> pojoClassses, Response response) {
                for (int i = 0; i < pojoClassses.size(); i++) {
                    tempDb.Insert(pojoClassses.get(i).getPlaceName(), pojoClassses.get(i).getCityName());
                }


                tempDb.Select(listCity);
                spinnerCity.setAdapter(adapterCity);


                progressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                progressDialog.dismiss();
            }
        });

spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        tempDb.SelectArea(listArea, listCity.get(position));
        spinnerArea.setAdapter(adapterArea);
        getCity = listCity.get(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
});

        spinnerArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getVendorArea=listArea.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    button.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
switch (v.getId())
{
    case R.id.buttonSignup:





        if(editName.getText().toString().equals(""))
        {
            editName.setError("Enter Name");
        }
        else if(editContact.getText().toString().equals("") || editContact.getText().toString().length()<10)
        {
            editContact.setError("Enter Contact Number");
        }
        else if(editPassword.getText().toString().equals(""))
        {
editPassword.setError("Please Enter Password");
        }
        else {


            final ProgressDialog progressDialog = new ProgressDialog(SignUpScreen.this);
            progressDialog.setMessage("Please Wait...");
            progressDialog.show();
            mainRetrofitClass.funRetro().InsertData(editName.getText().toString(),
                    editContact.getText().toString(), "" + getCity, getGcmId,
                    getVendorArea, editPassword.getText().toString(),vendorAddress.getText().toString(), new Callback<String>() {
                        @Override
                        public void success(String s, Response response) {
                            if (s.equals("This vendor already exists")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(SignUpScreen.this);
                                builder.setTitle("Already Exists");
                                builder.setMessage("Vendor with this contact number is already exists,Please Try To Login");
                                builder.setPositiveButton("Login", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(SignUpScreen.this, Login_Screen.class);
                                        startActivity(intent);
                                    }
                                });


                                builder.create();
                                builder.show();
                            } else {
                                sharedPrefrenceClass.setCheckLoginAccess("true");
                                sharedPrefrenceClass.setGetPlace(getVendorArea);
                                sharedPrefrenceClass.setCityName(""+getCity);
                                Intent intent = new Intent(SignUpScreen.this, MainActivity.class);
                                startActivity(intent);
                                finish();

                            }

                            progressDialog.dismiss();

                        }

                        @Override
                        public void failure(RetrofitError retrofitError) {

                        }
                    });

        }




            break;
}
        }
    });

    }




    public class AsynGcm extends AsyncTask<Void,Void,Void>
    {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                getGcmId = googleCloudMessaging.register(projectNumber);
            }catch (Exception e)
            {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            }
    }
}
